drop table if exists `languages`;

create table `languages`
(
	`language_id`   bigint unsigned not null auto_increment,
	`iso_2_code`    char(2) not null,
	`iso_3_code`    char(3) not null,
	`original_name` varchar(128) not null,
	primary key     (`language_id`),
	key             `k_iso_2_code` (`iso_2_code` ),
	key             `k_iso_3_code` (`iso_3_code` )
);

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'ru',      'rus', 'Русский'    );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'uk',      'ukr', 'Українська' );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'be',      'bel', 'Беларуская' );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'la',      'lat', 'Latin'      );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'eo',      'epo', 'Esperanto'  );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'en',      'eng', 'English'    );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'de',      'deu', 'Deutsch'    );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'it',      'ita', 'Italiano'   );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'es',      'spa', 'Español'    );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'pt',      'por', 'Português'  );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'fr',      'fra', 'Français'   );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'zh',      'zho', '中文'       );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'ja',      'jpn', '日本語'     );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'ar',      'ara', 'العربية'    );

insert into `languages`
	       (iso_2_code, iso_3_code, original_name)
	values (      'hi',      'hin', 'मानक हिन्दी' );
