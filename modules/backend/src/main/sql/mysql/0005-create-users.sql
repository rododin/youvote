drop table if exists `users`;

create table `users`
(
	`user_id`            bigint        unsigned not null auto_increment,
	`username`           varchar ( 32)          not null               ,
	`email`              varchar (256)          not null               ,
	`password_signature` varchar ( 32)          not null               ,
	`first_name`         varchar (128)          not null               ,
	`last_name`          varchar (128)          not null               ,
	`birth_date`         date                                          ,
	`gender`             enum    ('Male', 'Female')                    ,
	`disabled`           int     (  1)          not null      default 0,

	primary key                         (`user_id`                    ),
	key                  `k_username`   (`username`                   ),
	key                  `k_email`      (`email`                      ),
	key                  `k_name`       (`last_name`, `first_name`    ),
	key                  `k_birth_date` (`birth_date`                 ),
	key                  `k_gender`     (`gender`                     ),
	key                  `k_disabled`   (`disabled`                   )
);

-- PLEASE NOTE: The default password used for the password signature generation is: testing

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('RodOdin' , 'rododin@youvote.com'   , 'ca2911b345b22a2ea275f132bd414be7', 'Rod'        , 'Odin'     , '1970.01.01', 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('OdinRod' , 'odinrod@youvote.com'   , '6378ab75b8cff58e37bd6798fe0d057' , 'Odin'       , 'Rod'      , '1970.01.01', 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('vader'   , 'vader@youvote.com'     , '7660be712831561ee1efa09022f9bff8', 'Darth'      , 'Vader'    , '1970.01.01', 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('padme'   , 'padme@youvote.com'     , 'da67585820e41da0486f85d1592a1f94', 'Padme'      , 'Amidala'  , '1970.01.01', 'Female', 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('aragorn' , 'aragorn@youvote.com'   , '61a2bfc0dd5545452817701a98cc49ec', 'Ara'        , 'Gorn'     , '1970.01.01', 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('lovelace', 'lovelace@youvote.com'  , '7fda61a3a7977eeafab70644aab6c44' , 'Love'       , 'Lace'     , '1970.01.01', 'Female', 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('putin'   , 'putin@youvote.com'     , 'ef26cba9d5e31ca38e8da9e7bdb42e9' , 'Владимир'   , 'Путин'    , null        , 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('stalin'  , 'stalin@youvote.com'    , 'b848b6de3f9e7a14f2974c183bdfe655', 'Иосиф'      , 'Сталин'   , null        , 'Male'  , 0         );

insert into `users`
	       (`username` , `email`                , `password_signature`              , `first_name` , `last_name`, `birth_date`, `gender`, `disabled`)
	values ('churchill', 'churchill@youvote.com', 'a156917d671341a69e61c51c9141a'   , 'Winston'    , 'Churchill', null        , 'Male'  , 0         );

insert into `users`
	       (`username`, `email`                 , `password_signature`              , `first_name`, `last_name`, `birth_date`, `gender`, `disabled`)
	values ('willis'  , 'willis@youvote.com'    , 'a7409014ce9b10a4b1fecf15c6763544', 'Bruce'     , 'Willis'   , null        , 'Male'  , 0         );
