drop table if exists `countries`;

create table `countries`
(
	`country_id`    bigint unsigned not null auto_increment,
	`iso_2_code`    char(2) not null,
	`iso_3_code`    char(3) not null,
	`original_name` varchar(256) not null,
	primary key     (`country_id`),
	key             `k_iso_2_code` (`iso_2_code` ),
	key             `k_iso_3_code` (`iso_3_code` )
);

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'RU',      'RUS', 'Россия'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'UA',      'UKR', 'Україна'    );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'BY',      'BLR', 'Беларусь'   );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name   )
	values (      'UN',      'UNA', 'United Nations');

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'US',      'USA', 'U.S.A.'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name  )
	values (      'GB',      'GBR', 'Great Britain');

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'DE',      'DEU', 'Deutschland');

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'IT',      'ITA', 'Italia'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'ES',      'ESP', 'España'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'PT',      'PRT', 'Portugal'   );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'FR',      'FRA', 'France'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name  )
	values (      'CN',      'CHN', '中华人民共和国');

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'JP',      'JPN', '日本国'     );

insert into `countries`
	       (iso_2_code, iso_3_code, original_name                  )
	values (      'AE',      'ARE', 'دولة الإمارات العربية المتحدة');

insert into `countries`
	       (iso_2_code, iso_3_code, original_name)
	values (      'IN',      'IND', 'भारत गणराज्य');
