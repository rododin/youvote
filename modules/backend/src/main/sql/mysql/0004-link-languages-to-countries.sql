drop table if exists `country_languages`;

create table `country_languages`
(
	`country_language_id`  bigint unsigned not null auto_increment,
	`country_id`           bigint unsigned not null,
	`language_id`          bigint unsigned not null,
	`default`              int( 1) not null,
	primary key           (`country_language_id`),
	key                   `fk_country_id`  (`country_id` ),
	key                   `fk_language_id` (`language_id`),
	constraint            `fk_country_id`  foreign key (`country_id` ) references `countries` (`country_id` ) on delete cascade,
	constraint            `fk_language_id` foreign key (`language_id`) references `languages` (`language_id`) on delete cascade
);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='RU'), (select distinct `language_id` from `languages` where `iso_2_code`='ru'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UA'), (select distinct `language_id` from `languages` where `iso_2_code`='uk'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UA'), (select distinct `language_id` from `languages` where `iso_2_code`='ru'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='BY'), (select distinct `language_id` from `languages` where `iso_2_code`='be'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='BY'), (select distinct `language_id` from `languages` where `iso_2_code`='ru'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='en'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='ru'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='es'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='zh'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='ar'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='UN'), (select distinct `language_id` from `languages` where `iso_2_code`='fr'), 0);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='US'), (select distinct `language_id` from `languages` where `iso_2_code`='en'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='GB'), (select distinct `language_id` from `languages` where `iso_2_code`='en'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='DE'), (select distinct `language_id` from `languages` where `iso_2_code`='de'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='IT'), (select distinct `language_id` from `languages` where `iso_2_code`='it'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='ES'), (select distinct `language_id` from `languages` where `iso_2_code`='es'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='PT'), (select distinct `language_id` from `languages` where `iso_2_code`='pt'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='FR'), (select distinct `language_id` from `languages` where `iso_2_code`='fr'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='CN'), (select distinct `language_id` from `languages` where `iso_2_code`='zh'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='JP'), (select distinct `language_id` from `languages` where `iso_2_code`='ja'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='AE'), (select distinct `language_id` from `languages` where `iso_2_code`='ar'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='IN'), (select distinct `language_id` from `languages` where `iso_2_code`='hi'), 1);

insert into `country_languages`
	(`country_id`, `language_id`, `default`)
	values ((select distinct `country_id` from `countries` where `iso_2_code`='IN'), (select distinct `language_id` from `languages` where `iso_2_code`='en'), 0);
