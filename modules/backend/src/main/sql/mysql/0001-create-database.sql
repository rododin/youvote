create schema if not exists `youvote`
	default character set = 'utf8';

use `youvote`;

create user 'youvote'@'localhost' identified by 'youvote';

grant all on `youvote`.* to 'youvote' identified by 'youvote' with grant option;


# create table if not exists `test`
# (
# 	`test_id`   int(10) unsigned not null auto_increment,
# 	`c_int`     int not null default 0,
# 	`c_float`   float,
# 	`c_varchar` varchar(100),
# 	`c_text`    text,
# 	primary key (`test_id`)
# );
#
# insert into `test`
# (c_int, c_float, c_varchar, c_text)
# 	values (    1,   1.123, 'Test-1' , 'Test table, test data');
#
# insert into `test`
# (c_int, c_float, c_varchar, c_text)
# 	values (    2,   2.456, 'Test-2' , 'Test table, test data');
#
# insert into `test`
# (c_int, c_float, c_varchar, c_text)
# 	values (    3,   3.789, 'Test-3' , 'Test table, test data');
