/*
 * CountryLanguage.java
 */

package org.yvmht.backend.entities;

/**
 * Represents a <code>{@link Language}</code>-to-<code>{@link Country}</code> link with additional info
 * associated with the link.
 *
 * @author Rod Odin
 */
public interface CountryLanguage
	extends Entity
{
	/**
	 * Returns the internal <code>{@link Country#getId() countryId}</code> (the <code>Country</code>'s primary key).
	 * @return unique non-<code>null</code> <code>countryId</code>
	 */
	public Long getCountryId();

		/**
		 * Returns the internal <code>{@link Language#getId() languageId}</code> (the <code>Language</code>'s primary key).
		 * @return unique non-<code>null</code> <code>languageId</code>
		 */
	public Long getLanguageId();

	/**
	 * Returns the flag saying whether the language is default for the country or not.
	 * @return non-<code>null</code> boolean value, <code>true</code> if the language is default, <code>false</code> otherwise
	 */
	public Boolean isDefault();
}
