/*
 * ClientSessionImpl.java
 */

package org.yvmht.backend.impl.entities;

import org.yvmht.backend.entities.ClientSession;
import org.yvmht.backend.entities.Country;
import org.yvmht.backend.entities.Language;
import org.yvmht.backend.entities.User;

/**
 * Description.
 * @author Rod Odin
 */
public class ClientSessionImpl
	implements ClientSession
{
// Constructors --------------------------------------------------------------------------------------------------------

	public ClientSessionImpl(final String id)
	{
		this.id = id;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public String getId()
	{
		return id;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(final User user)
	{
		this.user = user;
	}

	public Country getSelectedCountry()
	{
		return selectedCountry;
	}

	public void setSelectedCountry(final Country selectedCountry)
	{
		this.selectedCountry = selectedCountry;
	}

	public Language getSelectedLanguage()
	{
		return selectedLanguage;
	}

	public void setSelectedLanguage(final Language selectedLanguage)
	{
		this.selectedLanguage = selectedLanguage;
	}

// Other Methods -------------------------------------------------------------------------------------------------------

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		final ClientSessionImpl that = (ClientSessionImpl) o;
		return id.equals(that.id);

	}

	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	@Override
	public String toString()
	{
		final User user = this.user;
		final StringBuilder rv = new StringBuilder(getClass().getSimpleName());
		rv.append("{id=").append(id);
		rv.append(", user=").append(user != null ? user.getUsername() : null);
		rv.append(", selectedCountry=").append(selectedCountry);
		rv.append(", selectedLanguage=").append(selectedLanguage);
		rv.append('}');
		return rv.toString();
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final String id;
	private User user;
	private Country selectedCountry;
	private Language selectedLanguage;
}
