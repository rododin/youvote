/*
 * BillDraft.java
 */

package org.yvmht.backend.entities;

import java.util.Date;

/**
 * TODO: Review and redevelop.
 *
 * @author Rod Odin
 */
public interface BillDraft
	extends Entity
{
	//public static enum BillDraftConstraintViolation
	//{
	//	IllegalTitleFormat, TitleAlreadyExists, IllegalDescriptionFormat,NullAuthorFormat,NullCountryFormat
	//}
	//
	//public User getAuthor();
	////public void setAuthor(User author);
	//
	//public User getModerator();
	////public void setModerator(User moderator);
	//
	//public Country getCountry();
	////public void setCountry(Country country);
	//
	//public String getTitle();
	//public void setTitle(String title);
	//
	//public String getDescription();
	//public void setDescription(String description);
	//public String getShortDescription();
	//public void setShortDescription(String shortDescription);
	//
	//public Date getCreationDate();
	//public void setCreationDate(Date creationDate);
	//public String getCreateDateAsString();
	//public void setCreateDateAsString(String newCreationDateAsString);
	//
	//public Date getPublishDate();
	//public void setPublishDate(Date publishDate);
	//public String getPublishDateAsString();
	//public void setPublishDateAsString(String newPublishDateAsString);
	//
	//public int getVoteCounter();
	//public void setVoteCounter(int voteCounter);
	//
	//public int getAgainstCounter();
	//public void setAgainstCounter(int againstCounter);
	//
	//public int getAbstainCounter();
	//public void setAbstainCounter(int abstainCounter);
	//
	//public boolean isPostponed();
	//public void setPostponed(boolean postponed);
	//
	//public String getPostponeComment();
	//public void setPostponeComment(String postponeComment);
	//
	//public boolean isRejected();
	//public void setRejected(boolean rejected);
	//
	//public String getRejectComment();
	//public void setRejectComment(String rejectComment);
	//
	//public boolean isClosed();
	//public void setClosed(boolean closed);
}
