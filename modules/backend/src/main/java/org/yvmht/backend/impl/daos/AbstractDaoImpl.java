/*
 * AbstractBean.java
 */

package org.yvmht.backend.impl.daos;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yvmht.backend.daos.Dao;

/**
 * Introduces a useful base implementation of the <code>{@link org.yvmht.backend.daos.Dao}</code> interface.
 *
 * @author Rod Odin
 */
public abstract class AbstractDaoImpl
	implements Dao
{
// Constants -----------------------------------------------------------------------------------------------------------

	protected final static Logger LOG = LoggerFactory.getLogger(AbstractDaoImpl.class);

	protected static final long CACHED_DATA_UPDATE_INTERVAL = 60 * 1000L; // 1 hour

// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractDaoImpl(EntityManager entityManager)
	{
		this.entityManager = entityManager;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public EntityManager getEntityManager()
	{
		return entityManager;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final EntityManager entityManager;
}
