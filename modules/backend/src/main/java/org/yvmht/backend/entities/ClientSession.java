/*
 * UserSession.java
 */

package org.yvmht.backend.entities;

/**
 * Represents a client session.
 * Formally it keeps a set of user session attributes to be managed while some user is on site, and to be forgotten
 * after the user leaves the site.
 * <p/>
 * Please note: The client session doesn't reference any user if it represents guest user who has not still logged in.
 * <p/>
 * Please note: The client sessions are never stored to DB.
 *
 * @author Rod Odin
 */
public interface ClientSession
{
	/**
	 * @return <code>HttpSession</code> ID
	 */
	String getId();

	/**
	 * @return the <code>{@link User}</code> associated with the session or <code>null</code> if the user has not still logged in
	 */
	User getUser();

	void setUser(User user);

	/**
	 * @return The <code>{@link Country}</code> selected by the user in GUI to work with.
	 */
	Country getSelectedCountry();

	void setSelectedCountry(Country country);

	/**
	 * @return The <code>{@link Language}</code> selected by the user in GUI to work with.
	 */
	Language getSelectedLanguage();

	void setSelectedLanguage(Language language);
}
