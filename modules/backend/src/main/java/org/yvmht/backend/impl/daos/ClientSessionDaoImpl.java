/*
 * ClientSessionDaoImpl.java
 */

package org.yvmht.backend.impl.daos;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;

import org.yvmht.backend.daos.ClientSessionDao;
import org.yvmht.backend.entities.ClientSession;
import org.yvmht.backend.impl.entities.ClientSessionImpl;

/**
 * Description.
 * @author Rod Odin
 */
public class ClientSessionDaoImpl
	extends AbstractDaoImpl
	implements ClientSessionDao
{
// Constants -----------------------------------------------------------------------------------------------------------

	protected static final int CLIENT_SESSION_CACHE_SIZE = 10 * 1000;

// Constructors --------------------------------------------------------------------------------------------------------

	protected ClientSessionDaoImpl(EntityManager entityManager)
	{
		super (entityManager);
	}

// DAO Implementation --------------------------------------------------------------------------------------------------

	@Override
	public ClientSession getClientSession(final String sessionId)
	{
		synchronized (clientSessions)
		{
			ClientSession rv = clientSessions.get(sessionId);
			if (rv == null)
			{
				rv = new ClientSessionImpl(sessionId);
				clientSessions.put(sessionId, rv);
			}
			return rv;
		}
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final Map<String, ClientSession> clientSessions =
		new LinkedHashMap<String, ClientSession>(CLIENT_SESSION_CACHE_SIZE, CLIENT_SESSION_CACHE_SIZE / 0.75f + 1, true)
		{
			@Override
			protected boolean removeEldestEntry(final Entry<String, ClientSession> eldest)
			{
				return clientSessions.size() > CLIENT_SESSION_CACHE_SIZE;
			}
		};
}
