/*
 * ClientSessionDao.java
 */

package org.yvmht.backend.daos;

import org.yvmht.backend.entities.ClientSession;

/**
 * Manages the <code>{@link ClientSession}</code>s.
 * @author Rod Odin
 */
public interface ClientSessionDao
{
	ClientSession getClientSession(String sessionId);
}
