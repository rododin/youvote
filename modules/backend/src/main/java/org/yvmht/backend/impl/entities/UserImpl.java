/*
 * User.java
 */

package org.yvmht.backend.impl.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.yvmht.backend.entities.User;

/**
 * The default implementation for the <code>{@link User}</code> entity.
 *
 * @author Rod Odin
 */
@Entity
@Table(name = "users")
public class UserImpl
	extends AbstractEntityImpl
	implements User
{
// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public UserImpl()
	{
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "user_id", nullable = false, unique = true)
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Long getId()
	{
		return super.getId();
	}

	@Column(name = "username", nullable = false, unique = true)
	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
		setModified(true);
	}

	@Column(name = "email", nullable = false, unique = true)
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
		setModified(true);
	}

	@Transient
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
		setModified(true);
	}

	@Column(name = "password_signature", nullable = false)
	public String getPasswordSignature()
	{
		return passwordSignature;
	}

	public void setPasswordSignature(String passwordSignature)
	{
		this.passwordSignature = passwordSignature;
		setModified(true);
	}

	@Column(name = "first_name", nullable = true, unique = false)
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
		setModified(true);
	}

	@Column(name = "last_name", nullable = true, unique = false)
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
		setModified(true);
	}

	@Column(name = "birth_date", nullable = true, unique = false)
	public Date getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(final Date birthDate)
	{
		this.birthDate = birthDate;
	}

	@Column(name = "gender", columnDefinition = "enum", nullable = true, unique = false)
	public Gender getGender()
	{
		return gender;
	}

	public void setGender(final Gender gender)
	{
		this.gender = gender;
	}

	@Column(name = "disabled", columnDefinition = "int")
	public Boolean isDisabled()
	{
		return disabled;
	}

	public void setDisabled(Boolean disabled)
	{
		this.disabled = disabled;
		setModified(true);
	}

	@Transient
	public Boolean isLoggedIn()
	{
	  return loggedIn;
	}

	@Transient
	public void setLoggedIn(Boolean loggedIn)
	{
	  this.loggedIn = loggedIn;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = 5811250114570241327L;

	private String username;
	private String email;
	private String password;
	private String passwordSignature;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private Gender gender;
	private boolean disabled;
	private boolean loggedIn;
}
