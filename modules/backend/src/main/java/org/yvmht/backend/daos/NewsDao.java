package org.yvmht.backend.daos;

/**
 * TODO: Redevelop totally.
 */
public interface NewsDao
	extends Dao
{
//	/**
//	 * Checks if a News with the given <code>title</code> already exists in the underlying persistence storage
//	 * (say in DB). Please note, this method doesn't check the <code>code</code> format for validity.
//	 * @see #verifyNews(org.yvmht.backend.entities.News)
//	 * @param title the title of the potentially registered News to be checked
//	 * @return <code>true</code> if the News exists, <code>false</code> otherwise
//	 * @throws org.yvmht.backend.exceptions.PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public boolean isNewsExisting(String title) throws PersistenceException;
//
//	/**
//	 * Checks if a News with the given <code>id</code> already exists in the underlying persistence storage
//	 * (say in DB). Please note, this method doesn't check the <code>id</code> format for validity.
//	 * @see #verifyNews(org.yvmht.backend.entities.News)
//	 * @param id the id of the potentially registered News to be checked
//	 * @return <code>true</code> if the News exists, <code>false</code> otherwise
//	 * @throws org.yvmht.backend.exceptions.PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public boolean isNewsExisting(Long id) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the news record by the given <code>title</code>.
//	 * @param title the News title
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.News}</code> instance if the News exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public News getNews(String title) throws PersistenceException;
//
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the news record by the given <code>id</code>.
//	 * @param id the News id
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.News}</code> instance if the News exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public News getNews(Long id) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the BillDraft record by the given <code>title</code>.
//	 * //@param title the BillDraft title
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.BillDraft}</code> array of instances if the billDrafts exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public List<NewsImpl> getAllNews() throws PersistenceException;
//
//	/**
//	 * Creates a <code>{@link News}</code> instance and sets default values to its properties.
//	 * Please note, the created entity is not stored in the persistence storage (say in DB), you have to use
//	 * <code>{@link #verifyNews(News)}</code> and <code>{@link #addOrUpdateNews(News)}</code> for that.
//	 * @see #verifyNews(News)
//	 * @see #addOrUpdateNews(News)
//	 * @return non-<code>null</code> <code>{@link News}</code> instance
//	 */
//	public News createDefaultNews();
//
//	/**
//	 * Returns an integer that represents number of <code>{@link News}</code> instances in DB.
//	 * @see #getAllNews()
//	 * @see #getNews(Long)
//	 * @see #getNews(String)
//	 * @return <code>integer</code>. 0 - zero instances(DB for News is empty).Else - Number of instances in DB
//	 */
//	//public int getNewsCount();
//
//	/**
//	 * Verifies whether the given <code>{@link News news}</code> is ready to be stored/updated in the underlying data
//	 * storage (say in DB) or not. It returns <code>null</code> if there are no violated constraints.
//	 * @see #isNewsExisting(String)
//	 * @see #//addOrUpdateNews(News)
//	 * @param news the News to be checked
//	 * @return <code>null</code> or non-empty set of the violated constraints.
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public Set<News.NewsConstraintViolation> verifyNews(News news)throws PersistenceException;
//
//
//	/**
//	 * Adds or update the given <code>{@link News news}</code>'s entry in(to) the underlying persistence storage
//	 * (say in(to) DB) if possible, or throws the <code>{@link org.yvmht.backend.exceptions.NewsException}</code> otherwise.
//	 * Use <code>{@link #verifyNews(News)}</code> to check whether the news is ready to be stored or not.
//	 * @see #verifyNews(News)
//	 * @param news the News to be added/updated
//	 * @throws org.yvmht.backend.exceptions.NewsException if something is wrong with the provided <code>{@link News}</code> instance,
//	 *         e.g. if <code>title</code> is <code>null</code> or already exists.
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void addOrUpdateNews(News news) throws NewsException, PersistenceException;
//
//	/**
//	 * Disables (bans) the News specified by the given <code>title</code>.
//	 * //There is no way to remove a News totally, but it is possible to disable the News(temporary or forever).
//	 * @see #enableNews(String),
//	 * @param title the title of the News to be disabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void disableNews(String title) throws PersistenceException;
//
//	/**
//	 * Enables the News specified by the given <code>title</code>.
//	 * @see #disableNews(String)
//	 * @param title the title of the News to be enabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void enableNews(String title) throws PersistenceException;
//
//	/**
//	 * Disables (bans) the News specified by the given <code>id</code>.
//	 * //There is no way to remove a News totally, but it is possible to disable the News(temporary or forever).
//	 * @see #enableNews(String),
//	 * @param id the id of the News to be disabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void disableNews(Long id) throws PersistenceException;
//
//	/**
//	 * Enables the News specified by the given <code>id</code>.
//	 * @see #disableNews(String)
//	 * @param id the id of the News to be enabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void enableNews(Long id) throws PersistenceException;
//
//// Testing/debugging stuff ---------------------------------------------------------------------------------------------
//
//	/**
//	 * Just populates and returns a set of testing News titles.
//	 * This method returns a constant set, so it can be used for <code>{@link #checkAndPopulateTestNews(Set)}</code>.
//	 * @see #checkAndPopulateTestNews(Set)
//	 * @return non-<code>null</code> and non-empty set of testing titles
//	 */
//	public Set<String> getTestNewsTitles();
//
//	/**
//	 * The method checks whether the set of provided test BillDrafts presents or not,
//	 * and if no test BillDrafts presents for one of provided <code>titles</code>, it creates one and inserts it to DB.
//	 * @see #getTestNewsTitles()
//	 * @param titles the set of test BillDrafts titles to check for
//	 * @return non-<code>null</code> and non-empty map of test user accounts mapped by <code>{@link org.yvmht.backend.entities.Country#getIso2Code()}  code}</code>
//	 */
//	public Map<String, News> checkAndPopulateTestNews(Set<String> titles);
//
}
