/*
 * LanguageDao.java
 */

package org.yvmht.backend.daos;

import java.util.Map;

import org.yvmht.backend.entities.Language;
import org.yvmht.backend.exceptions.PersistenceException;

/**
 * Introduces a set of methods to access the <code>languages</code> table in the underlying database.
 *
 * @author Rod Odin
 */
public interface LanguageDao
	extends Dao
{
	/**
	 * Requests the underlying DB for all existing <code>Language</code> records to be returned as is
	 * (mapped by internal primary code).
	 *
	 * @return non-<code>null</code>, but empty or non-empty map of the <code>{@link Language}</code> instances
	 *         mapped by the <code>{@link Language#getId() languageId}</code>
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Map<Long, ? extends Language> getAllLanguages() throws PersistenceException;

	/**
	 * Requests the underlying DB for all existing <code>Language</code> records to be ordered by the
	 * ISO-639-1 2-character code.
	 *
	 * @return non-<code>null</code>, but empty or non-empty map of the <code>{@link Language}</code> instances
	 *         mapped by the <code>{@link Language#getIso2Code() isoCode}</code>
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Map<String, ? extends Language> getAllLanguagesOrderedByIso2() throws PersistenceException;

	/**
	 * Requests the underlying DB for the <code>Language</code> record by the given internal <code>languageId</code>.
	 *
	 * @param languageId the internal Language's ID
	 * @return non-<code>null</code> <code>{@link Language}</code> instance if the language exists,
	 *         or <code>null</code> otherwise
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Language getLanguage(Long languageId) throws PersistenceException;

	/**
	 * Requests the underlying DB for the <code>Language</code> record by the given <code>iso2Code</code>.
	 *
	 * @param iso2Code the Language's ISO-639-1 code
	 * @return non-<code>null</code> <code>{@link Language}</code> instance if the language exists,
	 *         or <code>null</code> otherwise
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Language getLanguage(String iso2Code) throws PersistenceException;

// Testing/debugging stuff ---------------------------------------------------------------------------------------------

	///**
	// * Just populates and returns a default set of non-persisted <code>{@link Language}</code>s
	// * mapped by the ISO-639-1 2-character codes, and to be used for testings/debugging purposes.
	// * Please note, this method does NOT access the database for reading or updating anything.
	// *
	// * @return non-<code>null</code> and non-empty map of the <code>{@link Language}</code> instances
	// */
	//public Map<String, ? extends Language> getTestLanguages();
}
