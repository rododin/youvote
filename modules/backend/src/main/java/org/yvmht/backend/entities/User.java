/*
 * User.java
 */

package org.yvmht.backend.entities;

import java.util.Date;

/**
 * Represents a user account.
 *
 * @author Rod Odin
 */
public interface User
	extends Entity
{
	/**
	 * Introduces business level user violation constraints.
	 */
	public static enum UserConstraintViolation
	{
		IllegalEmailFormat, EmailAlreadyExists, IllegalPasswordFormat,
	}

	public static enum Gender
	{
		Male, Female
	}

	public String getUsername();
	public void setUsername(String username);

	public String getPassword();
	public void setPassword(String password);

	public String getEmail();
	public void setEmail(String email);

	public String getFirstName();
	public void setFirstName(String firstName);

	public String getLastName();
	public void setLastName(String lastName);

	public Date getBirthDate();
	public void setBirthDate(Date birthDate);

	public Gender getGender();
	public void setGender(Gender gender);

	public Boolean isDisabled();
	//public void setDisabled(Boolean disabled);

	public Boolean isLoggedIn();
	//public void setLoggedIn(Boolean loggedIn);
}
