/*
 * CountryLanguageImpl.java
 */

package org.yvmht.backend.impl.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.yvmht.backend.entities.CountryLanguage;

/**
 * Description.
 *
 * @author Rod Odin
 */
@Entity
@Table(name = "country_languages")
public class CountryLanguageImpl
	extends AbstractEntityImpl
	implements CountryLanguage
{
// Constants -----------------------------------------------------------------------------------------------------------

// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public CountryLanguageImpl()
	{
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "country_language_id", nullable = false, unique = true)
	//@GeneratedValue(generator = "increment")
	//@GenericGenerator(name = "increment", strategy = "increment")
	public Long getId()
	{
		return super.getId();
	}

	@Column(name = "country_id", nullable = false, unique = false)
	public Long getCountryId()
	{
		return countryId;
	}

	public void setCountryId(final Long countryId)
	{
		this.countryId = countryId;
	}

	@Column(name = "language_id", nullable = false, unique = false)
	public Long getLanguageId()
	{
		return languageId;
	}

	public void setLanguageId(final Long languageId)
	{
		this.languageId = languageId;
	}

	@Column(name = "default", columnDefinition = "int", nullable = false, unique = false)
	public Boolean isDefault()
	{
		return isDefault;
	}

	public void setDefault(final Boolean isDefault)
	{
		this.isDefault = isDefault;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private Long countryId;
	private Long languageId;
	private Boolean isDefault;
}
