/*
 * LanguageImpl.java
 */

package org.yvmht.backend.impl.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.yvmht.backend.entities.Language;

/**
 * Description.
 *
 * @author Rod Odin
 */
@Entity
@Table(name = "languages")
public class LanguageImpl
	extends AbstractEntityImpl
	implements Language
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final LanguageImpl DEFAULT_LANGUAGE = new LanguageImpl("en", "eng", "English");

// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public LanguageImpl()
	{
	}

	public LanguageImpl(final String iso2Code, final String iso3Code, final String originalName)
	{
		setIso2Code(iso2Code);
		setIso3Code(iso3Code);
		setOriginalName(originalName);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "language_id", nullable = false, unique = true)
	//@GeneratedValue(generator = "increment")
	//@GenericGenerator(name = "increment", strategy = "increment")
	public Long getId()
	{
		return super.getId();
	}

	@Column(name = "iso_2_code", columnDefinition = "char", nullable = false, unique = true)
	public String getIso2Code()
	{
		return iso2Code;
	}

	public void setIso2Code(String iso2Code)
	{
		this.iso2Code = iso2Code;
	}

	@Column(name = "iso_3_code", columnDefinition = "char", nullable = false, unique = true)
	public String getIso3Code()
	{
		return iso3Code;
	}

	public void setIso3Code(String iso3Code)
	{
		this.iso3Code = iso3Code;
	}

	@Column(name = "original_name", nullable = false)
	public String getOriginalName()
	{
		return originalName;
	}

	public void setOriginalName(String originalName)
	{
		this.originalName = originalName;
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public String toString()
	{
		return iso2Code + " - " + originalName;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = 8695531144215089617L;

	private String iso2Code;
	private String iso3Code;
	private String originalName;
}
