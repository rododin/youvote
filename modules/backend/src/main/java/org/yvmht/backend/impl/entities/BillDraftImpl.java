/*
 * BillDraftImpl.java
 */

package org.yvmht.backend.impl.entities;

import org.hibernate.annotations.GenericGenerator;
import org.yvmht.backend.entities.BillDraft;

import javax.persistence.*;
import java.util.Date;

/**
 * TODO: Review and redevelop.
 *
 * @author Rod Odin
 */
//@Entity
//@Table(name = "bill_drafts")
public class BillDraftImpl
	  //extends AbstractEntityImpl
	  //implements BillDraft
{
//// Constructors --------------------------------------------------------------------------------------------------------
//
//	/**
//	 * Required for Hibernate.
//	 */
//	public BillDraftImpl()
//	{
//	}
//
//// Getters/Setters -----------------------------------------------------------------------------------------------------
//
//	@Id
//	@Column(name = "bill_draft_id", nullable = false, unique = true)
//	@GeneratedValue(generator = "increment")
//	@GenericGenerator(name = "increment", strategy = "increment")
//	public Long getId()
//	{
//		return super.getId();
//	}
//
//	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
//	@JoinColumn(name = "author_user_id")
//	public UserImpl getAuthor()
//	{
//		return author;
//	}
//
//	public void setAuthor(UserImpl author)
//	{
//		this.author = author;
//	}
//
//
//	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
//	@JoinColumn(name = "moderator_user_id")
//	public UserImpl getModerator()
//	{
//		return moderator;
//	}
//
//	public void setModerator(UserImpl moderator)
//	{
//		this.moderator = moderator;
//	}
//
//	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,CascadeType.REMOVE})
//	@JoinColumn(name = "country_id")
//	public CountryImpl getCountry()
//	{
//		return country;
//	}
//
//	public void setCountry(CountryImpl country)
//	{
//		this.country = country;
//	}
//
//	@Column(name = "title", nullable = false, unique = true)
//	public String getTitle()
//	{
//		return title;
//	}
//
//	public void setTitle(String title)
//	{
//		this.title = title;
//	}
//
//	@Column(name = "description", nullable = false)
//	public String getDescription()
//	{
//		return description;
//	}
//
//	public void setDescription(String description)
//	{
//		this.description = description;
//		if (description.length()>50)
//			this.shortDescription = description.substring(1,50)+"...";
//		else
//			this.shortDescription = description.substring(1,description.length());
//	}
//
//	@Transient
//	public String getShortDescription()
//	{
//		return shortDescription;
//	}
//
//	public void setShortDescription(String shortDescription)
//	{
//		this.shortDescription = shortDescription;
//	}
//
//
//	@Column(name = "creation_date", nullable = false)
//	public Date getCreationDate()
//	{
//		return creationDate;
//	}
//
//	public void setCreationDate(Date creationDate)
//	{
//		this.createDateAsString = String.format("%td/%<tm/%<tY", creationDate);
//		this.creationDate = creationDate;
//	}
//
//
//	@Column(name = "publish_date")//, nullable = false
//	public Date getPublishDate()
//	{
//		return publishDate;
//	}
//
//	public void setPublishDate(Date publishDate)
//	{
//		this.publishDateAsString = String.format("%td/%<tm/%<tY", publishDate);
//		this.publishDate = publishDate;
//	}
//
//	@Column(name = "vote_counter")
//	public int getVoteCounter()
//	{
//		return voteCounter;
//	}
//
//	public void setVoteCounter(int voteCounter)
//	{
//		this.voteCounter = voteCounter;
//	}
//
//	@Column(name = "against_counter")
//	public int getAgainstCounter()
//	{
//		return againstCounter;
//	}
//
//	public void setAgainstCounter(int againstCounter)
//	{
//		this.againstCounter = againstCounter;
//	}
//
//	@Column(name = "abstain_counter")
//	public int getAbstainCounter()
//	{
//		return abstainCounter;
//	}
//
//	public void setAbstainCounter(int abstainCounter)
//	{
//		this.abstainCounter = abstainCounter;
//	}
//
//	@Column(name = "postponed")
//	public boolean isPostponed()
//	{
//		return postponed;
//	}
//
//	public void setPostponed(boolean postponed)
//	{
//		this.postponed = postponed;
//	}
//
//	@Column(name = "postponed_comment")
//	public String getPostponeComment()
//	{
//		return postponeComment;
//	}
//
//	public void setPostponeComment(String postponeComment)
//	{
//		this.postponeComment = postponeComment;
//	}
//
//	@Column(name = "rejected")
//	public boolean isRejected()
//	{
//		return rejected;
//	}
//
//	public void setRejected(boolean rejected)
//	{
//		this.rejected = rejected;
//	}
//
//	@Column(name = "rejected_comment")
//	public String getRejectComment()
//	{
//		return rejectComment;
//	}
//
//	public void setRejectComment(String rejectComment)
//	{
//		this.rejectComment = rejectComment;
//	}
//
//	@Column(name = "closed")
//	public boolean isClosed()
//	{
//		return closed;
//	}
//
//	public void setClosed(boolean closed)
//	{
//		this.closed = closed;
//	}
//
//	@Transient
//	public String getCreateDateAsString()
//	{
//		return createDateAsString;
//	}
//	public void setCreateDateAsString(String newCreationDateAsString)
//	{
//		createDateAsString = newCreationDateAsString;
//	}
//
//	@Transient
//	public String getPublishDateAsString()
//	{
//		return publishDateAsString;
//	}
//	public void setPublishDateAsString(String newPublishDateAsString)
//	{
//		publishDateAsString = newPublishDateAsString;
//	}
//
//// Attributes ----------------------------------------------------------------------------------------------------------
//
//	private static final long serialVersionUID = -2821123447510597217L;
//
//	private long id;
//	private UserImpl author;
//	private UserImpl moderator;
//	private CountryImpl country;
//
//	private String title;
//	private String description;
//	private String shortDescription;
//
//	private Date creationDate;
//	private String createDateAsString;
//	private Date publishDate;
//	private String publishDateAsString;
//
//	private int voteCounter;
//	private int againstCounter;
//	private int abstainCounter;
//
//	private boolean postponed;
//	private String postponeComment;
//
//	private boolean rejected;
//	private String rejectComment;
//
//	private boolean closed;
}
