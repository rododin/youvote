/*
 * Bean.java
 */

package org.yvmht.backend.daos;

import javax.persistence.EntityManager;

/**
 * Introduces an abstract DAO controller.
 *
 * @author Rod Odin
 */
public interface Dao
{
	EntityManager getEntityManager();
}
