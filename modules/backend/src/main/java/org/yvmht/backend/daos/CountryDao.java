/*
 * CountryDao.java
 */

package org.yvmht.backend.daos;

import java.util.Map;

import org.yvmht.backend.entities.Country;
import org.yvmht.backend.exceptions.PersistenceException;

/**
 * Introduces a set of methods to access the <code>countries</code> table in the underlying database.
 *
 * @author Rod Odin
 */
public interface CountryDao
	extends Dao
{
	/**
	 * Requests the underlying DB for all existing <code>Country</code> records to be returned as is
	 * (mapped by internal primary code).
	 *
	 * @return non-<code>null</code>, but empty or non-empty map of the <code>{@link Country}</code> instances
	 *         mapped by the <code>{@link Country#getId()} countryId}</code>
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Map<Long, ? extends Country> getAllCountries() throws PersistenceException;

	/**
	 * Requests the underlying DB for all existing <code>Country</code> records to be ordered by the
	 * ISO-3166 2-character code.
	 *
	 * @return non-<code>null</code>, but empty or non-empty map of the <code>{@link Country}</code> instances
	 *         mapped by the <code>{@link Country#getIso2Code()} iso2Code}</code>
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Map<String, ? extends Country> getAllCountriesOrderedByIso2() throws PersistenceException;

	/**
	 * Requests the underlying DB for the <code>Country</code> record by the given <code>countryId</code>.
	 *
	 * @param countryId the Country's internal ID
	 * @return non-<code>null</code> <code>{@link Country}</code> instance if the country exists,
	 *         or <code>null</code> otherwise
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Country getCountry(Long countryId) throws PersistenceException;

	/**
	 * Requests the underlying DB for the <code>Country</code> record by the given <code>iso2Code</code>.
	 *
	 * @param iso2Code the Country's ISO 3166 2-character code
	 * @return non-<code>null</code> <code>{@link Country}</code> instance if the country exists,
	 *         or <code>null</code> otherwise
	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
	 */
	public Country getCountry(String iso2Code) throws PersistenceException;

// Testing/debugging stuff ---------------------------------------------------------------------------------------------

	///**
	// * Just populates and returns a default set of non-persisted <code>{@link Country}</code>ies
	// * mapped by the ISO 3166 2-character codes, and to be used for testings/debugging purposes.
	// * Please note, this method does NOT access the database for reading or updating anything.
	// *
	// * @return non-<code>null</code> and non-empty map of the <code>{@link Country}</code> instances
	// */
	//public Map<String, ? extends Country> getTestCountries();
}
