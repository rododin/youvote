/*
 * BillDraftManager.java
 */

package org.yvmht.backend.impl.daos;

/**
 * TODO: Review and redevelop.
 *
 * @author Rod Odin
 */
public abstract class BillDraftDaoImpl
	//extends AbstractDaoImpl
	//implements BillDraftDao
{
//// Constructors --------------------------------------------------------------------------------------------------------
//
//	protected BillDraftDaoImpl(EntityManager entityManager)
//	{
//		super(entityManager);
//	}
//
//// Dao Implementation -------------------------------------------------------------------------------------------------
//
//	public boolean isBillDraftExisting(String title) throws PersistenceException
//	{
//		return getBillDraft(title)!=null;
//	}
//
//	public boolean isBillDraftExisting(Long id) throws PersistenceException
//	{
//		return getBillDraft(id)!=null;
//	}
//
//	public BillDraftImpl getBillDraft(String title) throws PersistenceException
//	{
//		BillDraftImpl rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			if (title == null)
//				throw new NullPointerException("title is null");
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = entityManager.createQuery("select b from BillDraftImpl as b where b.title = ?1", BillDraftImpl.class).setParameter(1, title).getSingleResult();
//			transaction.commit();
//			if (rv != null)
//			{
//				rv.setNew(false);
//				rv.setModified(false);
//			}
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("BillDraft doesn't exist: title=" + title);
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get BillDraft by title: title=" + title;
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//
//	}
//
//	public List<BillDraftImpl> getAllBillDrafts() throws PersistenceException
//	{
//		Long i;
//		List<BillDraftImpl> rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = new ArrayList<BillDraftImpl>();
//
//			// _____________OPTIMIZE IT ! __________________   BEGIN
//			// Getting List<BillDraft>
//			i=1L;
//			while ((i<4000000 ) && (entityManager.createQuery("select b from BillDraftImpl as b where b.id = ?1", BillDraftImpl.class).setParameter(1, i).getSingleResult()!=null) )
//			{
//				rv.add(entityManager.createQuery("select b from BillDraftImpl as b where b.id = ?1 ", BillDraftImpl.class).setParameter(1, i).getSingleResult());
//				i++;
//			}
//			//##############################################   END
//			transaction.commit();
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("BillDraft doesn't exist (NoResultException)");
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get BillDraft List";
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//	}
//
//	public BillDraftImpl getBillDraft(Long id) throws PersistenceException
//	{
//		BillDraftImpl rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			if (id == null)
//				throw new NullPointerException("id is null");
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = entityManager.createQuery("select b from BillDraftImpl as b where b.id = ?1", BillDraftImpl.class).setParameter(1, id).getSingleResult();
//			transaction.commit();
//			if (rv != null)
//			{
//				rv.setNew(false);
//				rv.setModified(false);
//			}
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("BillDraft doesn't exist: id=" + id);
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get BillDraft by id: id=" + id;
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//	}
//
//	public BillDraftImpl  createDefaultBillDraft()
//	{
//		final BillDraftImpl  rv = new BillDraftImpl ();
//		//rv.set...
//		return rv;
//	}
//
//	public Set<BillDraft.BillDraftConstraintViolation> verifyBillDraft(BillDraft billDraft)throws PersistenceException
//	{
//		final Set<BillDraft.BillDraftConstraintViolation> rv = new HashSet<>();
//		if (billDraft.getTitle() == null )
//			rv.add(BillDraft.BillDraftConstraintViolation.IllegalTitleFormat);
//		if (isBillDraftExisting(billDraft.getTitle()))
//			rv.add(BillDraft.BillDraftConstraintViolation.TitleAlreadyExists);
//		if (billDraft.getDescription()== null)
//			rv.add(BillDraft.BillDraftConstraintViolation.IllegalDescriptionFormat);
//		//if (billDraft.getAuthor() == null)
//		//	rv.add(BillDraft.BillDraftConstraintViolation.NullAuthorFormat);
//		//if (billDraft.getCountry() == null)
//		//	rv.add(BillDraft.BillDraftConstraintViolation.NullCountryFormat);
//
//		//if (billDraft.getPassword() == null || billDraft.getPassword().isEmpty())
//		//	rv.add(BillDraft.UserConstraintViolation.IllegalPasswordFormat);
//		return rv.isEmpty() ? null : rv;
//	}
//
//	public void addOrUpdateBillDraft(BillDraft billDraft) throws BillDraftException, PersistenceException
//	{
//		synchronized (billDraft)
//		{
//			if (billDraft.isNew() || billDraft.isModified())
//			{
//				final Set<BillDraft.BillDraftConstraintViolation> constrainViolations = verifyBillDraft(billDraft);
//				if (constrainViolations != null)
//				{
//					final String msg = "Unable to add/update billDraft: billDraft=" + billDraft + ", constrainViolations=" + constrainViolations;
//					LOG.error(msg);
//					throw new BillDraftException(msg, constrainViolations.iterator().next());
//				}
//				EntityTransaction transaction = null;
//				try
//				{
//					final BillDraftImpl billDraftImpl = (BillDraftImpl) billDraft;
//					billDraftImpl.setTitle(billDraft.getTitle());
//
//					//billDraftImpl.setAuthor((UserImpl) billDraft.getAuthor());		 // ? - How it should properly described
//					//billDraftImpl.setAuthor((UserImpl)billDraftImpl.getAuthor());		 // ? - How it should properly described
//					//billDraftImpl.setAuthor();
//					//billDraftImpl.setCountry((CountryImpl) billDraft.getCountry()); // ? - How it should properly described
//					billDraftImpl.setCreationDate(new Date());
//					billDraftImpl.setPublishDate(new Date()); // Must be different
//					final EntityManager entityManager = getEntityManager();
//					LOG.info("------------------------------------------------------------------");
//					LOG.info("BillDraft="+billDraft);
//					LOG.info("billDraftImpl="+billDraftImpl);
//					LOG.info("------------------------------------------------------------------");
//					transaction = entityManager.getTransaction();
//					transaction.begin();
//					if (billDraft.isNew())
//						entityManager.persist(billDraft);
//					else
//						entityManager.merge(billDraft);
//					transaction.commit();
//
//					billDraftImpl.setNew(false);
//					billDraftImpl.setModified(false);
//				}
//				catch (EntityExistsException x)
//				{
//					if (transaction != null)
//						transaction.rollback();
//					final String msg = "Unable to add/update billDraft: billDraft=" + billDraft + ", constrainViolations=" + BillDraft.BillDraftConstraintViolation.TitleAlreadyExists;
//					LOG.error(msg, x);
//					throw new BillDraftException(msg, BillDraft.BillDraftConstraintViolation.TitleAlreadyExists);
//				}
//				catch (Exception x)
//				{
//					if (transaction != null)
//						transaction.rollback();
//					final String msg = "Unable to add/update billDraft: billDraft=" + billDraft;
//					LOG.error(msg, x);
//					throw new PersistenceException(msg, x);
//				}
//			}
//		}
//	}
//	public void disableBillDraft(String title) throws PersistenceException
//	{
//		doSetDisabled(title, true);
//	}
//
//	public void enableBillDraft(String title) throws PersistenceException
//	{
//		doSetDisabled(title, false);
//	}
//
//	public void disableBillDraft(Long id) throws PersistenceException
//	{
//		doSetDisabled(id, true);
//	}
//
//	public void enableBillDraft(Long id) throws PersistenceException
//	{
//		doSetDisabled(id, false);
//	}
//
//	// Internal Logic ------------------------------------------------------------------------------------------------------
//
//	protected void doSetDisabled(String title, boolean disabled) throws PersistenceException
//	{
//		BillDraftImpl billDraft = getBillDraft(title);
//		if (billDraft  != null)
//		{
//			try
//			{
//				synchronized (billDraft)
//				{
//					billDraft.setPostponed(disabled);  // Changed from User.setDisabled()
//					final EntityManager entityManager = getEntityManager();
//					entityManager.getTransaction().begin();
//					entityManager.merge(billDraft );
//					entityManager.getTransaction().commit();
//					billDraft.setNew(false);
//					billDraft.setModified(false);
//				}
//			}
//			catch (Exception x)
//			{
//				final String msg = "Unable to disable/enable billDraft: billDraft=" + billDraft + ", disabled=" + disabled;
//				LOG.error(msg, x);
//				throw new PersistenceException(msg, x);
//			}
//		}
//	}
//
//	protected void doSetDisabled(Long id, boolean disabled) throws PersistenceException
//	{
//		BillDraftImpl billDraft = getBillDraft(id);
//		if (billDraft  != null)
//		{
//			try
//			{
//				synchronized (billDraft)
//				{
//					billDraft.setPostponed(disabled);  // Changed from User.setDisabled()
//					final EntityManager entityManager = getEntityManager();
//					entityManager.getTransaction().begin();
//					entityManager.merge(billDraft );
//					entityManager.getTransaction().commit();
//					billDraft.setNew(false);
//					billDraft.setModified(false);
//				}
//			}
//			catch (Exception x)
//			{
//				final String msg = "Unable to disable/enable billDraft: billDraft=" + billDraft + ", disabled=" + disabled;
//				LOG.error(msg, x);
//				throw new PersistenceException(msg, x);
//			}
//		}
//	}
//
//
//	public Set<String> getTestBillDraftsTitles()
//	{
//		final Set<String> titles = new LinkedHashSet<>();
//		for (int i = 0; i < 10; i++)
//			titles.add("test #" + i + " :Title \"yv-mht"+i+"\"");
//		return titles;
//	}
//
//	public Map<String, BillDraft> checkAndPopulateTestBillDrafts(Set<String> titles) {
//		final Map<String, BillDraft> rv = new LinkedHashMap<>();
//		try
//		{
//			// Checking the billdrafts, if someone doesn't present, creating him and adding to DB
//			// Inserting the billdraft to the result map
//			for (String title : titles)
//			{
//				BillDraftImpl billDraft = getBillDraft(title);
//				if (billDraft == null)
//				{
//					final String[] titleParts = title.split("@");
//					//final UserImpl author = titleParts[0];
//					//final CountryImpl country = titleParts[1];
//					final String creationDate = titleParts[2];
//					final String description = titleParts[3];
//
//					//final CountryImpl currentCountry = (CountryImpl) BackEnd.getDefaultBackend().//
//					billDraft = createDefaultBillDraft();
//
//					billDraft.setTitle(title);
//
//					//billDraft.setAuthor(author);
//					//billDraft.setCountry(country);
//					//billDraft.setCreationDate();
//					//billDraft.setDescription();
//					billDraft.setPostponed(false);
//
//					addOrUpdateBillDraft(billDraft);
//					billDraft = getBillDraft(title);
//				}
//				rv.put(title, billDraft);
//			}
//		}
//		catch (Exception x)
//		{
//			LOG.error("BillDraft checking/population error", x);
//		}
//		return rv;
//	}
}
