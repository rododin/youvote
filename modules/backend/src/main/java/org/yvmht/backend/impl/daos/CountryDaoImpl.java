/*
 * CountryDaoImpl.java
 */

package org.yvmht.backend.impl.daos;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import org.yvmht.backend.daos.CountryDao;
import org.yvmht.backend.daos.LanguageDao;
import org.yvmht.backend.entities.Country;
import org.yvmht.backend.entities.Language;
import org.yvmht.backend.exceptions.PersistenceException;
import org.yvmht.backend.impl.entities.CountryImpl;
import org.yvmht.backend.impl.entities.CountryLanguageImpl;

/**
 * The default implementation for <code>{@link CountryDao}</code>.
 *
 * @author Rod Odin
 */
public abstract class CountryDaoImpl
	extends AbstractDaoImpl
	implements CountryDao
{
// Constants -----------------------------------------------------------------------------------------------------------

	protected static final long CACHED_DATA_UPDATE_INTERVAL = AbstractDaoImpl.CACHED_DATA_UPDATE_INTERVAL;

// Constructors --------------------------------------------------------------------------------------------------------

	protected CountryDaoImpl(EntityManager entityManager, LanguageDao languageDao)
	{
		super (entityManager);
		this.languageDao = languageDao;
	}

// DAO Implementation --------------------------------------------------------------------------------------------------

	@Override
	public Map<Long, ? extends Country> getAllCountries()
		throws PersistenceException
	{
		updateCachedData();
		return new HashMap<>(allCountriesById);
	}

	@Override
	public Map<String, ? extends Country> getAllCountriesOrderedByIso2()
		throws PersistenceException
	{
		updateCachedData();
		return new LinkedHashMap<>(allCountriesByIso2);
	}

	@Override
	public Country getCountry(final Long countryId)
		throws PersistenceException
	{
		return getAllCountries().get(countryId);
	}

	@Override
	public Country getCountry(final String iso2Code)
		throws PersistenceException
	{
		return getAllCountriesOrderedByIso2().get(iso2Code);
	}

// Internal Logic ------------------------------------------------------------------------------------------------------

	protected void updateCachedData()
		throws PersistenceException
	{
		final long currentTime = System.currentTimeMillis();
		final long lastCachedDataUpdateTimestamp = this.lastCachedDataUpdateTimestamp.get();
		if (currentTime - lastCachedDataUpdateTimestamp >= CACHED_DATA_UPDATE_INTERVAL)
		{
			if (this.lastCachedDataUpdateTimestamp.compareAndSet(lastCachedDataUpdateTimestamp, currentTime))
			{
				final LinkedHashMap<String, CountryImpl> allCountriesByIso2 = new LinkedHashMap<>();
				final HashMap<Long, CountryImpl> allCountriesById = new HashMap<>();
				EntityTransaction transaction = null;
				try
				{
					final Map<Long, ? extends Language> allLanguages = languageDao.getAllLanguages();
					final EntityManager entityManager = getEntityManager();
					transaction = entityManager.getTransaction();
					transaction.begin();
					final List<CountryImpl> countries = entityManager.createQuery("select c from CountryImpl as c order by c.iso2Code", CountryImpl.class).getResultList();
					if (countries != null)
					{
						for (CountryImpl country : countries)
						{
							country.setNew(false);
							country.setModified(false);
							allCountriesById.put(country.getId(), country);
							allCountriesByIso2.put(country.getIso2Code(), country);

							final Long countryId = country.getId();
							try
							{
								final List<CountryLanguageImpl> countryLanguages = entityManager.createQuery("select cl from CountryLanguageImpl as cl where cl.countryId=?1", CountryLanguageImpl.class).setParameter(1, countryId).getResultList();
								final Map<String, Language> languages = new LinkedHashMap<>();
								Language defaultLanguage = null;
								for (CountryLanguageImpl countryLanguage : countryLanguages)
								{
									final Long languageId = countryLanguage.getLanguageId();
									final Language language = allLanguages.get(languageId);
									if (language != null)
									{
										languages.put(language.getIso2Code(), language);
										if (defaultLanguage == null && countryLanguage.isDefault())
											defaultLanguage = language;
									}
									else
										LOG.warn("No Result: CountryDao.updateCachedData: No Languages defined for the Country: countryId=" + countryId);
								}
								country.setDefaultLanguage(defaultLanguage);
								country.setLanguages(languages);
							}
							catch (NoResultException x)
							{
								LOG.warn("No Result: CountryDao.updateCachedData: No Languages defined for the Country: countryId=" + countryId, x);
							}
						}
					}
					transaction.commit();
				}
				catch (NoResultException x)
				{
					if (transaction != null)
						transaction.rollback();
					LOG.warn("No Result: CountryDao.updateCachedData: No Countries found!", x);
				}
				catch (Exception x)
				{
					if (transaction != null)
						transaction.rollback();
					final String msg = "Query error: CountryDao.updateCachedData";
					LOG.error(msg, x);
					throw new PersistenceException(msg, x);
				}
				this.allCountriesById = allCountriesById;
				this.allCountriesByIso2 = allCountriesByIso2;
			}
		}
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final LanguageDao languageDao;

	private final AtomicLong lastCachedDataUpdateTimestamp = new AtomicLong(System.currentTimeMillis() - CACHED_DATA_UPDATE_INTERVAL - 1);
	private volatile HashMap<Long, CountryImpl> allCountriesById = new HashMap<>();
	private volatile LinkedHashMap<String, CountryImpl> allCountriesByIso2 = new LinkedHashMap<>();
}
