/*
 * Language.java
 */

package org.yvmht.backend.entities;

/**
 * Represent a language.
 *
 * @author Rod Odin
 */
public interface Language
	extends Entity
{
	/**
	 * Returns the language ISO 639-1 2-character code.
	 * @return unique non-<code>null</code> language code
	 */
	public String getIso2Code();

	/**
	 * Returns the language ISO 639-2 3-character code.
	 * @return unique non-<code>null</code> language code
	 */
	public String getIso3Code();

	/**
	 * Returns the language name in its original language, e.g. "Русский" for the Russian language.
	 * @return non-<code>null</code> language name
	 */
	public String getOriginalName();
}
