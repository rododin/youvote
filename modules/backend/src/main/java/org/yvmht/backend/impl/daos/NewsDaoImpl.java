package org.yvmht.backend.impl.daos;

/**
 * TODO: Review and redevelop.
 */
public abstract class NewsDaoImpl
	//extends AbstractDaoImpl
	//implements NewsDao
{
//// Constructors --------------------------------------------------------------------------------------------------------
//
//	protected NewsDaoImpl(EntityManager entityManager)
//	{
//		super(entityManager);
//	}
//
//	// Dao Implementation -------------------------------------------------------------------------------------------------
//
//	public boolean isNewsExisting(String title) throws PersistenceException
//	{
//		return getNews(title)!=null;
//	}
//
//	public boolean isNewsExisting(Long id) throws PersistenceException
//	{
//		return getNews(id)!=null;
//	}
//
//
//	public News getNews(String title) throws PersistenceException
//	{
//		NewsImpl rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			if (title == null)
//				throw new NullPointerException("title is null");
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = entityManager.createQuery("select n from NewsImpl as n where n.title = ?1", NewsImpl.class).setParameter(1, title).getSingleResult();
//			transaction.commit();
//			if (rv != null)
//			{
//				rv.setNew(false);
//				rv.setModified(false);
//			}
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("News doesn't exist: title=" + title);
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get News by title: title=" + title;
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//
//	}
//
//	public News getNews(Long id) throws PersistenceException
//	{
//		NewsImpl rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			if (id == null)
//				throw new NullPointerException("id is null");
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = entityManager.createQuery("select n from NewsImpl as n where n.id = ?1", NewsImpl.class).setParameter(1, id).getSingleResult();
//			transaction.commit();
//			if (rv != null)
//			{
//				rv.setNew(false);
//				rv.setModified(false);
//			}
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("News doesn't exist: id=" + id);
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get News by id: id=" + id;
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//
//	}
//
//	public List<NewsImpl> getAllNews() throws PersistenceException
//	{
//		Long i;
//		List<NewsImpl> rv = null;
//		EntityTransaction transaction = null;
//		try
//		{
//			final EntityManager entityManager = getEntityManager();
//			transaction = entityManager.getTransaction();
//			transaction.begin();
//			rv = new ArrayList<NewsImpl>();
//
//			// _____________OPTIMIZE IT ! __________________   BEGIN
//			// Getting List<BillDraft>
//			i=1L;
//			while ((i<4000000 ) && (entityManager.createQuery("select b from NewsImpl as b where b.id = ?1", NewsImpl.class).setParameter(1, i).getSingleResult()!=null) )
//			{
//				rv.add(entityManager.createQuery("select b from NewsImpl as b where b.id = ?1 ", NewsImpl.class).setParameter(1, i).getSingleResult());
//				i++;
//			}
//			//##############################################   END
//			transaction.commit();
//		}
//		catch (NoResultException x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			LOG.warn("News doesn't exist (NoResultException)");
//		}
//		catch (Exception x)
//		{
//			if (transaction != null)
//				transaction.rollback();
//			final String msg = "Unable to get News List";
//			LOG.error(msg, x);
//			throw new PersistenceException(msg, x);
//		}
//		return rv;
//	}
//
//	public News createDefaultNews()
//	{
//			final NewsImpl  rv = new NewsImpl ();
//			//rv.set...
//			return rv;
//	}
//
//	public Set<News.NewsConstraintViolation> verifyNews(News news)throws PersistenceException
//	{
//		final Set<News.NewsConstraintViolation> rv = new HashSet<>();
//		if (news.getTitle() == null )
//			rv.add(News.NewsConstraintViolation.TitleAlreadyExists);
//		//if (isNewsExisting(news.getId()))										//??
//		//	rv.add(News.NewsConstraintViolation.NewsAlreadyExists);
//
//		//if (billDraft.getPassword() == null || billDraft.getPassword().isEmpty())
//		//	rv.add(BillDraft.UserConstraintViolation.IllegalPasswordFormat);
//		return rv.isEmpty() ? null : rv;
//	}
//
//
//	public void addOrUpdateNews(News news) throws NewsException, PersistenceException
//	{
//		synchronized (news)
//		{
//			if (news.isNew() || news.isModified())
//			{
//				final Set<News.NewsConstraintViolation> constrainViolations = verifyNews(news);
//				if (constrainViolations != null)
//				{
//					final String msg = "Unable to add/update news: news=" + news + ", constrainViolations=" + constrainViolations;
//					LOG.error(msg);
//					throw new NewsException(msg, constrainViolations.iterator().next());
//				}
//				EntityTransaction transaction = null;
//				try
//				{
//					final NewsImpl newsImpl = (NewsImpl) news;
//					//billDraftImpl.setPasswordSignature(PasswordSignatureGenerator.createSignature(billDraftImpl.getEmail(), billDraftImpl.getPassword()));
//					newsImpl.setTitle(news.getTitle());
//					newsImpl.setAuthor((UserImpl) news.getAuthor());			//???
//					newsImpl.setCountry((CountryImpl)news.getCountry());		//???
//					newsImpl.setPublishDate(new Date());
//					newsImpl.setDescription(news.getDescription());
//					newsImpl.setHelpfullCounter(0);
//					newsImpl.setHelplessCounter(0);
//					newsImpl.setType(news.getType());
//					final EntityManager entityManager = getEntityManager();
//					transaction = entityManager.getTransaction();
//					transaction.begin();
//					if (news.isNew())
//						entityManager.persist(news);
//					else
//						entityManager.merge(news);
//					transaction.commit();
//
//					newsImpl.setNew(false);
//					newsImpl.setModified(false);
//				}
//				catch (EntityExistsException x)
//				{
//					if (transaction != null)
//						transaction.rollback();
//					final String msg = "Unable to add/update news: news=" + news + ", constrainViolations=" + News.NewsConstraintViolation.NewsAlreadyExists;
//					LOG.error(msg, x);
//					throw new NewsException(msg,News.NewsConstraintViolation.NewsAlreadyExists);
//				}
//				catch (Exception x)
//				{
//					if (transaction != null)
//						transaction.rollback();
//					final String msg = "Unable to add/update news: news=" + news;
//					LOG.error(msg, x);
//					throw new PersistenceException(msg, x);
//				}
//			}
//		}
//	}
//
//	public void disableNews(String title) throws PersistenceException
//	{
//		doSetDisabled(title, true);
//	}
//
//	public void enableNews(String title) throws PersistenceException
//	{
//		doSetDisabled(title, false);
//	}
//
//	public void disableNews(Long id) throws PersistenceException
//	{
//		doSetDisabled(id, true);
//	}
//
//	public void enableNews(Long id) throws PersistenceException
//	{
//		doSetDisabled(id, false);
//	}
//
//	protected void doSetDisabled(String title, boolean disabled) throws PersistenceException
//	{
//		NewsImpl news = (NewsImpl)getNews(title);
//		if (news != null)
//		{
//			try
//			{
//				synchronized (news)
//				{
//					//country.set/(disabled);  // Changed from User.setDisabled()		// !!!????
//					final EntityManager entityManager = getEntityManager();
//					entityManager.getTransaction().begin();
//					entityManager.merge(news);
//					entityManager.getTransaction().commit();
//					news.setNew(false);
//					news.setModified(false);
//				}
//			}
//			catch (Exception x)
//			{
//				final String msg = "Unable to disable/enable news: news=" + news+ ", disabled=" + disabled;
//				LOG.error(msg, x);
//				throw new PersistenceException(msg, x);
//			}
//		}
//	}
//	protected void doSetDisabled(Long id, boolean disabled) throws PersistenceException
//	{
//		NewsImpl news = (NewsImpl)getNews(id);
//		if (news != null)
//		{
//			try
//			{
//				synchronized (news)
//				{
//					//country.set/(disabled);  // Changed from User.setDisabled()		// !!!????
//					final EntityManager entityManager = getEntityManager();
//					entityManager.getTransaction().begin();
//					entityManager.merge(news);
//					entityManager.getTransaction().commit();
//					news.setNew(false);
//					news.setModified(false);
//				}
//			}
//			catch (Exception x)
//			{
//				final String msg = "Unable to disable/enable news: news=" + news+ ", disabled=" + disabled;
//				LOG.error(msg, x);
//				throw new PersistenceException(msg, x);
//			}
//		}
//	}
//
//// Testing/debugging stuff ---------------------------------------------------------------------------------------------
//
//	public Set<String> getTestNewsTitles(){
//		final Set<String> titles = new LinkedHashSet<>();
//		for (int i = 0; i < 10; i++)
//			titles.add("test #" + i + " :Title \"yv-mht"+i+"\"");
//		return titles;
//	}
//
//	public Map<String, News> checkAndPopulateTestNews(Set<String> titles)
//	{
//
//		final Map<String, News> rv = new LinkedHashMap<>();
//		try
//		{
//			// Checking the news, if someone doesn't present, creating him and adding to DB
//			// Inserting the news to the result map
//			for (String title : titles)
//			{
//				NewsImpl news = (NewsImpl)getNews(title);
//				if (news == null)
//				{
//					final String[] titleParts = title.split("@");
//					//final UserImpl author = titleParts[0];
//					//final CountryImpl country = titleParts[1];
//					final String creationDate = titleParts[2];
//					final String description = titleParts[3];
//
//					//final CountryImpl currentCountry = (CountryImpl) BackEnd.getDefaultBackend().//
//					news = (NewsImpl)createDefaultNews();
//
//					news.setTitle(title);
//
//					//billDraft.setAuthor(author);
//					//billDraft.setCountry(country);
//					//billDraft.setCreationDate();
//					//billDraft.setDescription();
//					news.setNew(true);
//
//					addOrUpdateNews(news);
//					news = (NewsImpl)getNews(title);
//				}
//				rv.put(title, news);
//			}
//		}
//		catch (Exception x)
//		{
//			LOG.error("BillDraft checking/population error", x);
//		}
//		return rv;
//	}
//
//
}
