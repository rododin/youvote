/*
 * ContryImpl.java
 */

package org.yvmht.backend.impl.entities;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.yvmht.backend.entities.Country;
import org.yvmht.backend.entities.Language;

/**
 * Description.
 *
 * @author Rod Odin
 */
@Entity
@Table(name = "countries")
public class CountryImpl
	extends AbstractEntityImpl
	implements Country
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final CountryImpl DEFAULT_COUNTRY = new CountryImpl("US", "USA", LanguageImpl.DEFAULT_LANGUAGE);

// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public CountryImpl()
	{
	}

	public CountryImpl(final String iso2Code, final String iso3Code, final LanguageImpl defaultLanguage)
	{
		setIso2Code(iso2Code);
		setIso3Code(iso3Code);
		setDefaultLanguage(defaultLanguage);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "country_id", nullable = false, unique = true)
	//@GeneratedValue(generator = "increment")
	//@GenericGenerator(name = "increment", strategy = "increment")
	public Long getId()
	{
		return super.getId();
	}

	@Column(name = "iso_2_code", columnDefinition = "char", nullable = false, unique = true)
	public String getIso2Code()
	{
		return iso2Code;
	}

	public void setIso2Code(String iso2Code)
	{
		this.iso2Code = iso2Code;
	}

	@Column(name = "iso_3_code", columnDefinition = "char", nullable = false, unique = true)
	public String getIso3Code()
	{
		return iso3Code;
	}

	public void setIso3Code(String iso3Code)
	{
		this.iso3Code = iso3Code;
	}

	@Column(name = "original_name", nullable = false, unique = false)
	public String getOriginalName()
	{
		return originalName;
	}

	public void setOriginalName(final String originalName)
	{
		this.originalName = originalName;
	}

	@Transient
	public Language getDefaultLanguage()
	{
		return defaultLanguage;
	}

	public void setDefaultLanguage(Language defaultLanguage)
	{
		this.defaultLanguage = defaultLanguage;
	}

	@Transient
	public Map<String, ? extends Language> getLanguages()
	{
		return languages;
	}

	public void setLanguages(final Map<String, ? extends Language> languages)
	{
		this.languages = languages;
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public String toString()
	{
		return iso2Code + " - " + originalName;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = -2395975529242590923L;

	private String                          iso2Code;
	private String                          iso3Code;
	private String                          originalName;
	private Language                        defaultLanguage;
	private Map<String, ? extends Language> languages;
}
