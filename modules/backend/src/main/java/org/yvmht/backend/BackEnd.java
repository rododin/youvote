/*
 * BackEnd.java
 */

package org.yvmht.backend;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.yvmht.backend.daos.ClientSessionDao;
import org.yvmht.backend.daos.CountryDao;
import org.yvmht.backend.daos.Dao;
import org.yvmht.backend.daos.LanguageDao;
import org.yvmht.backend.daos.UserDao;
import org.yvmht.backend.impl.daos.ClientSessionDaoImpl;
import org.yvmht.backend.impl.daos.CountryDaoImpl;
import org.yvmht.backend.impl.daos.LanguageDaoImpl;
import org.yvmht.backend.impl.daos.UserDaoImpl;

/**
 * The BackEnd singleton.
 *
 * @author Rod Odin
 */
public class BackEnd
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected BackEnd()
	{
		entityManagerFactory = Persistence.createEntityManagerFactory("org.yvmht.backend.jpa");
		defaultEntityManager = entityManagerFactory.createEntityManager();

		final LanguageDaoImpl      languageDao      = new LanguageDaoImpl     (defaultEntityManager             ) {};
		final CountryDaoImpl       countryDao       = new CountryDaoImpl      (defaultEntityManager, languageDao) {};
		final ClientSessionDaoImpl clientSessionDao = new ClientSessionDaoImpl(defaultEntityManager             ) {};
		final UserDaoImpl          userDao          = new UserDaoImpl         (defaultEntityManager             ) {};

		daos.put(LanguageDao     .class.getName(), languageDao     );
		daos.put(CountryDao      .class.getName(), countryDao      );
		daos.put(ClientSessionDao.class.getName(), clientSessionDao);
		daos.put(UserDao         .class.getName(), userDao         );
	}

// Initialization/Finalization -----------------------------------------------------------------------------------------

	protected void finalize() throws Throwable
	{
		defaultEntityManager.close();
		entityManagerFactory.close();
		super.finalize();
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public static BackEnd getDefaultBackend()
	{
		if (DEFAULT_BACKEND.get() == null)
			DEFAULT_BACKEND.compareAndSet(null, new BackEnd());
		return DEFAULT_BACKEND.get();
	}

	public LanguageDao getLanguageDao()
	{
		return (LanguageDao) daos.get(LanguageDao.class.getName());
	}

	public CountryDao getCountryDao()
	{
		return (CountryDao) daos.get(CountryDao.class.getName());
	}

	public ClientSessionDao getClientSessionDao()
	{
		return (ClientSessionDao) daos.get(ClientSessionDao.class.getName());
	}

	public UserDao getUserDao()
	{
		return (UserDao) daos.get(UserDao.class.getName());
	}

	// Attributes ----------------------------------------------------------------------------------------------------------

	private static final AtomicReference<BackEnd> DEFAULT_BACKEND = new AtomicReference<BackEnd>();

	private EntityManagerFactory entityManagerFactory;
	private EntityManager defaultEntityManager;

	private Map<String, Dao> daos = new HashMap<>();
}
