/*
 * BillDraftManager.java
 */

package org.yvmht.backend.daos;

/**
 * TODO: Redevelop totally.
 *
 * @author Rod Odin
 */
public interface BillDraftDao
	extends Dao
{
//	/**
//	 * Checks if a BillDraft with the given <code>title</code> already exists in the underlying persistence storage
//	 * (say in DB). Please note, this method doesn't check the <code>title</code> format for validity.
//	 * @see #verifyBillDraft(org.yvmht.backend.entities.BillDraft)
//	 * @param title the title of the potentially registered BillDraft to be checked
//	 * @return <code>true</code> if the BillDraft exists, <code>false</code> otherwise
//	 * @throws org.yvmht.backend.exceptions.PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public boolean isBillDraftExisting(String title) throws PersistenceException;
//
//	/**
//	 * Checks if a BillDraft with the given <code>id</code> already exists in the underlying persistence storage
//	 * (say in DB). Please note, this method doesn't check the <code>id</code> format for validity.
//	 * @see #verifyBillDraft(org.yvmht.backend.entities.BillDraft)
//	 * @param id the id of the potentially registered BillDraft to be checked
//	 * @return <code>true</code> if the BillDraft exists, <code>false</code> otherwise
//	 * @throws org.yvmht.backend.exceptions.PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public boolean isBillDraftExisting(Long id) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the BillDraft record by the given <code>title</code>.
//	 * @param title the BillDraft title
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.BillDraft}</code> instance if the billDraft exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public BillDraft getBillDraft(String title) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the BillDraft record by the given <code>id</code>.
//	 * @param id the BillDraft id
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.BillDraft}</code> instance if the billDraft exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public BillDraft getBillDraft(Long id) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the BillDraft record by the given <code>title</code>.
//	 * //@param title the BillDraft title
//	 * @return non-<code>null</code> <code>{@link org.yvmht.backend.entities.BillDraft}</code> array of instances if the billDrafts exists,
//	 *         or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public List<BillDraftImpl> getAllBillDrafts() throws PersistenceException;
//
//
//	/**
//	 * Creates a <code>{@link BillDraft}</code> instance and sets default values to its properties.
//	 * Please note, the created entity is not stored in the persistence storage (say in DB), you have to use
//	 * <code>{@link #verifyBillDraft(BillDraft)}</code> and <code>{@link #addOrUpdateBillDraft(BillDraft)}</code> for that.
//	 * @see #verifyBillDraft(BillDraft)
//	 * @see #addOrUpdateBillDraft(BillDraft)
//	 * @return non-<code>null</code> <code>{@link BillDraft}</code> instance
//	 */
//	public BillDraft createDefaultBillDraft();
//
//	/**
//	 * Verifies whether the given <code>{@link BillDraft billDraft}</code> is ready to be stored/updated in the underlying data
//	 * storage (say in DB) or not. It returns <code>null</code> if there are no violated constraints.
//	 * @see #isBillDraftExisting(String)
//	 * @see #//addOrUpdateBillDraft(BillDraft)
//	 * @param billDraft the BillDraft to be checked
//	 * @return <code>null</code> or non-empty set of the violated constraints.
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public Set<BillDraft.BillDraftConstraintViolation> verifyBillDraft(BillDraft billDraft)throws PersistenceException;
//
//
//	/**
//	 * Adds or update the given <code>{@link BillDraft billDraft}</code>'s entry in(to) the underlying persistence storage
//	 * (say in(to) DB) if possible, or throws the <code>{@link org.yvmht.backend.exceptions.BillDraftException}</code> otherwise.
//	 * Use <code>{@link #verifyBillDraft(BillDraft)}</code> to check whether the billDrafts is ready to be stored or not.
//	 * @see #verifyBillDraft(BillDraft)
//	 * @param billDraft the BillDraft to be added/updated
//	 * @throws org.yvmht.backend.exceptions.BillDraftException if something is wrong with the provided <code>{@link BillDraft}</code> instance,
//	 *         e.g. if <code>title</code> is <code>null</code> or already exists.
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void addOrUpdateBillDraft(BillDraft billDraft) throws BillDraftException, PersistenceException;
//
//	/**
//	 * Disables (bans) the BillDraft specified by the given <code>title</code>.
//	 * //There is no way to remove a billDraft totally, but it is possible to disable the billDraft (temporary or forever).
//	 * @see #enableBillDraft(String),
//	 * @param title the title of the BillDraft to be disabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void disableBillDraft(String title) throws PersistenceException;
//
//	/**
//	 * Disables (bans) the BillDraft specified by the given <code>id</code>.
//	 * //There is no way to remove a user totally, but it is possible to disable the user account (temporary or forever).
//	 * @see #enableBillDraft(String),
//	 * @param id the id of the BillDraft to be disabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void disableBillDraft(Long id) throws PersistenceException;
//
//	/**
//	 * Enables the BillDraft specified by the given <code>title</code>.
//	 * @see #disableBillDraft(String)
//	 * @param title the title of the BillDraft to be enabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void enableBillDraft(String title) throws PersistenceException;
//
//	/**
//	 * Enables the BillDraft specified by the given <code>id</code>.
//	 * @see #disableBillDraft(String)
//	 * @param id the id of the BillDraft to be enabled
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	public void enableBillDraft(Long id) throws PersistenceException;
//
//// Testing/debugging stuff ---------------------------------------------------------------------------------------------
//
//	/**
//	 * Just populates and returns a set of testing BillDrafts titles.
//	 * This method returns a constant set, so it can be used for <code>{@link #checkAndPopulateTestBillDrafts(Set)}</code>.
//	 * @see #checkAndPopulateTestBillDrafts(Set)
//	 * @return non-<code>null</code> and non-empty set of testing titles
//	 */
//	public Set<String> getTestBillDraftsTitles();
//
//	/**
//	 * The method checks whether the set of provided test BillDrafts presents or not,
//	 * and if no test BillDrafts presents for one of provided <code>titles</code>, it creates one and inserts it to DB.
//	 * @see #getTestBillDraftsTitles()
//	 * @param titles the set of test BillDrafts titles to check for
//	 * @return non-<code>null</code> and non-empty map of test user accounts mapped by <code>{@link BillDraft#getTitle() title}</code>
//	 */
//	public Map<String, BillDraft> checkAndPopulateTestBillDrafts(Set<String> titles);
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the user record by the given <code>email</code> and
//	 * <code>password</code>. If the user exists and the password is correct, it logs the user in and returns appropriate
//	 * <code>{@link BillDraft}</code> instance marked as logged-in.
//	 * @see #logoutUser(BillDraft)
//	 * @see User#isLoggedIn()
//	 * @param email the user email
//	 * @param password the password of the user to be logged-in.
//	 * @return non-<code>null</code> <code>{@link User}</code> instance if the user exists
//	 *         and the <code>password</code> is correct, or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	//public bool sendBillDraft(String title, String password) throws PersistenceException;
//
//	/**
//	 * Requests the underlying persistence storage (say the DB) for the user record by the given <code>email</code> and
//	 * <code>password</code>. If the user exists and the password is correct, it logs the user in and returns appropriate
//	 * <code>{@link BillDraft}</code> instance marked as logged-in.
//	 * @see #logoutUser(BillDraft)
//	 * @see User#isLoggedIn()
//	 * @param email the user email
//	 * @param password the password of the user to be logged-in.
//	 * @return non-<code>null</code> <code>{@link User}</code> instance if the user exists
//	 *         and the <code>password</code> is correct, or <code>null</code> otherwise
//	 * @throws PersistenceException if the system cannot access the persistence storage or another problem occurs
//	 */
//	//public User loginUser(String email, String password) throws PersistenceException;
//
//	/**
//	 * Logs the <code>{@link User user}</code> out.
//	 * Normally it doesn't access the underlying persistence storage, but just removes the user from internal runtime maps,
//	 * however it marks the <code>{@link User user}</code> as logged-out.
//	 * @see #loginUser(String, String)
//	 * @see User#isLoggedIn()
//	 * @param user the user to be logged-out
//	 * @throws UserException if the {@link User user instance} is broken or doesn't exist
//	 */
//	//public void logoutUser(User user) throws UserException;
//
}
