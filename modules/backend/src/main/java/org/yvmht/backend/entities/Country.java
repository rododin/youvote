/*
 * Country.java
 */

package org.yvmht.backend.entities;

import java.util.Map;

/**
 * Represents a country.
 *
 * @author Rod Odin
 */
public interface Country
	extends Entity
{
	/**
	 * Returns the ISO 3166 2-character country code.
	 * @return unique non-<code>null</code> country code
	 */
	public String getIso2Code();

	/**
	 * Returns the ISO 3166 3-character country code.
	 * @return unique non-<code>null</code> country code
	 */
	public String getIso3Code();

	/**
	 * Returns the country name in its original language, e.g. "Россия" for Russia.
	 * @return non-<code>null</code> country name
	 */
	public String getOriginalName();

	/**
	 * Returns the default country language.
	 * @return non-<code>null</code> <code>{@link Language}</code> instance
	 */
	public Language getDefaultLanguage();

	/**
	 * Returns all languages supported by the country.
	 * @return non-<code>null</code> and non-empty set of the <code>{@link Language}</code> instances mapped
	 *         by the language <code>{@link Language#getIso2Code() iso2Code}</code>s.
	 */
	public Map<String, ? extends Language> getLanguages();
}
