/*
 * LanguageDaoImpl.java
 */

package org.yvmht.backend.impl.daos;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import org.yvmht.backend.daos.LanguageDao;
import org.yvmht.backend.entities.Language;
import org.yvmht.backend.exceptions.PersistenceException;
import org.yvmht.backend.impl.entities.LanguageImpl;

/**
 * The default implementation for <code>{@link LanguageDao}</code>.
 *
 * @author Rod Odin
 */
public abstract class LanguageDaoImpl
	extends AbstractDaoImpl
	implements LanguageDao
{
// Constants -----------------------------------------------------------------------------------------------------------

	protected static final long CACHED_DATA_UPDATE_INTERVAL = AbstractDaoImpl.CACHED_DATA_UPDATE_INTERVAL;

// Constructors --------------------------------------------------------------------------------------------------------

	protected LanguageDaoImpl(EntityManager entityManager)
	{
		super (entityManager);
	}

// DAO Implementation --------------------------------------------------------------------------------------------------

	@Override
	public Map<Long, ? extends Language> getAllLanguages()
		throws PersistenceException
	{
		updateCachedData();
		return new HashMap<>(allLanguagesById);
	}

	@Override
	public Map<String, ? extends Language> getAllLanguagesOrderedByIso2()
		throws PersistenceException
	{
		updateCachedData();
		return new LinkedHashMap<>(allLanguagesByIso2);
	}

	@Override
	public Language getLanguage(final Long languageId)
		throws PersistenceException
	{
		return getAllLanguages().get(languageId);
	}

	@Override
	public Language getLanguage(final String iso2Code)
		throws PersistenceException
	{
		return getAllLanguagesOrderedByIso2().get(iso2Code);
	}

// Internal Logic ------------------------------------------------------------------------------------------------------

	protected void updateCachedData()
		throws PersistenceException
	{
		final long currentTime = System.currentTimeMillis();
		final long lastCachedDataUpdateTimestamp = this.lastCachedDataUpdateTimestamp.get();
		if (currentTime - lastCachedDataUpdateTimestamp >= CACHED_DATA_UPDATE_INTERVAL)
		{
			if (this.lastCachedDataUpdateTimestamp.compareAndSet(lastCachedDataUpdateTimestamp, currentTime))
			{
				final HashMap<Long, LanguageImpl> allLanguagesById = new HashMap<>();
				final LinkedHashMap<String, LanguageImpl> allLanguagesByIso2 = new LinkedHashMap<>();
				EntityTransaction transaction = null;
				try
				{
					final EntityManager entityManager = getEntityManager();
					transaction = entityManager.getTransaction();
					transaction.begin();
					final List<LanguageImpl> languages = entityManager.createQuery("select l from LanguageImpl as l order by l.iso2Code", LanguageImpl.class).getResultList();
					if (languages != null)
					{
						for (LanguageImpl language : languages)
						{
							language.setNew(false);
							language.setModified(false);
							allLanguagesById  .put(language.getId(), language);
							allLanguagesByIso2.put(language.getIso2Code(), language);
						}
					}
					transaction.commit();
				}
				catch (NoResultException x)
				{
					if (transaction != null)
						transaction.rollback();
					LOG.warn("No Result: LanguageDao.updateCachedData", x);
				}
				catch (Exception x)
				{
					if (transaction != null)
						transaction.rollback();
					final String msg = "Query error: LanguageDao.updateCachedData";
					LOG.error(msg, x);
					throw new PersistenceException(msg, x);
				}
				this.allLanguagesById = allLanguagesById;
				this.allLanguagesByIso2 = allLanguagesByIso2;
			}
		}
	}

	// Attributes ----------------------------------------------------------------------------------------------------------

	private final AtomicLong lastCachedDataUpdateTimestamp = new AtomicLong(System.currentTimeMillis() - CACHED_DATA_UPDATE_INTERVAL - 1);
	private volatile HashMap<Long, LanguageImpl> allLanguagesById = new HashMap<>();
	private volatile LinkedHashMap<String, LanguageImpl> allLanguagesByIso2 = new LinkedHashMap<>();
}
