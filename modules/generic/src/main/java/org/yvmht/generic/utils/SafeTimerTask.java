/*
 * SafeTimerTask.java
 */

package org.yvmht.generic.utils;

import java.util.TimerTask;

import org.yvmht.generic.Constants;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class SafeTimerTask
	extends TimerTask
{
	@Override
	public void run()
	{
		try
		{
			doRun();
		}
		catch (Exception x)
		{
			Constants.LOG.warn("TimerTask failed", x);
		}
	}

	protected abstract void doRun();
}
