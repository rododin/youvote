/*
 * Constants.java
 */

package org.yvmht.generic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Description.
 * @author Rod Odin
 */
public class Constants
{
	public final static Logger LOG = LoggerFactory.getLogger(Constants.class);
}
