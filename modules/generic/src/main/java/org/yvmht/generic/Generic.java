/*
 * Generic.java
 */

package org.yvmht.generic;

import org.yvmht.generic.utils.PasswordSignatureGenerator;

/**
 * Main routine stub.
 *
 * @author Rod Odin
 */
public class Generic
{
	public static void main(String[] args)
	{
		System.out.println("Username/Password: MD5 Signature");

		final String[] usernamesAndPasswords = new String[]
			{
				"RodOdin/testing",
				"OdinRod/testing",
				"vader/testing",
				"padme/testing",
				"aragorn/testing",
				"lovelace/testing",
				"putin/testing",
				"stalin/testing",
				"churchill/testing",
				"willis/testing",
			};

		for (String up : usernamesAndPasswords)
		{
			final String[] strings = up.split("/");
			System.out.println(up + ": " + PasswordSignatureGenerator.createSignature(strings[0], strings[1]));
		}
	}
}
