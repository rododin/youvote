/*
 * CountryWrapper.java
 */

package org.yvmht.web.page_controller.entity_wrappers;

import org.yvmht.backend.entities.Country;

/**
 * Description.
 * @author Rod Odin
 */
public class CountryExt
{
// Constants -----------------------------------------------------------------------------------------------------------

	private static final String COUNTRY_IMAGE_PATH = "resources/img/country_flags/flag_";
	private static final String COUNTRY_IMAGE_EXT  = ".png";

// Constructors --------------------------------------------------------------------------------------------------------

	public CountryExt(final Country country)
	{
		this.country = country;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public Country getCountry()
	{
		return country;
	}

	public String getCountryImage()
	{
		return COUNTRY_IMAGE_PATH + country.getIso2Code() + COUNTRY_IMAGE_EXT;
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public String toString()
	{
		return country.toString();
	}


// Attributes ----------------------------------------------------------------------------------------------------------

	private final Country country;
}
