package org.yvmht.web.page_controller.utils.filters;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: arck  * Date: 23.12.11 * Time: 0:05
 */
public interface Filter <ObjectType, FilterValue extends ObjectType>{

// Constants ---------------------------------------------------------------------------------------

// Methods -----------------------------------------------------------------------------------------

		/**
		 * Filters the given <code>sourceList</code> by comparing each of its object with
		 * <code>filterValue</code> according to the rules defined by <code>criteria</code>.
		 *
		 * @param sourceList a list of <code>ObjectType</code> elements to be filtered
		 * @param filterValue etalon object probably of the same class or sub-class of the objects
		 *                    in the <code>sourceList</code>.
		 * @param criteria a set of filtering rules
		 * @param comparator comparator used to compare the objects
		 * @return filtered list probably of zero size, but not null any way
		 */
		public List<ObjectType> filter( List<ObjectType>                     sourceList
											  , FilterValue                          filterValue
											  , Criteria                             criteria
											  , Comparator<ObjectType, FilterValue>  comparator
		);

}// public interface filter

// EOF
