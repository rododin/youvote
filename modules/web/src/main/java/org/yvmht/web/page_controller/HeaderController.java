/*
 * HeaderController.java
 */

package org.yvmht.web.page_controller;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import javax.faces.application.Application;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpSession;

import org.yvmht.backend.BackEnd;
import org.yvmht.backend.daos.ClientSessionDao;
import org.yvmht.backend.daos.CountryDao;
import org.yvmht.backend.entities.ClientSession;
import org.yvmht.backend.entities.Country;
import org.yvmht.backend.entities.Language;
import org.yvmht.backend.exceptions.PersistenceException;
import org.yvmht.web.page_controller.entity_wrappers.CountryExt;

/**
 * Description.
 * @author Rod Odin
 */
@SessionScoped
@ManagedBean(name = "headerController")
public class HeaderController
	extends AbstractController
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final String DEFAULT_RESOURCE_BUNDLE_NAME = HeaderController.class.getPackage().getName() + ".CommonPageText";

// Constructors --------------------------------------------------------------------------------------------------------

	public HeaderController()
	{
		super (DEFAULT_RESOURCE_BUNDLE_NAME);
	}

// Inner Classes -------------------------------------------------------------------------------------------------------

	public class CountryConverter
		implements Converter
	{
		@Override
		public Object getAsObject(final FacesContext context, final UIComponent component, final String value)
		{
			CountryExt rv = null;
			if (value != null)
			{
				final String iso2Code = value.substring(0, 2);
				final Map<String, CountryExt> countries = HeaderController.this.countries.get();
				rv = countries != null ? countries.get(iso2Code) : null;
			}
			return rv;
		}

		@Override
		public String getAsString(final FacesContext context, final UIComponent component, final Object value)
		{
			//LOG.info("CountryConverter.getAsString: value=" + value + ", valueClass=" + (value != null ? value.getClass().getName() : null));
			final CountryExt country = value instanceof CountryExt ? (CountryExt) value : null;
			return country != null ? country.getCountry().getIso2Code() + " - " + country.getCountry().getOriginalName() : null;
		}
	}

	public class LanguageConverter
		implements Converter
	{
		@Override
		public Object getAsObject(final FacesContext context, final UIComponent component, final String value)
		{
			Object rv = null;
			if (value != null)
			{
				final String iso2Code = value.substring(0, 2);
				final CountryExt selectedCountry = HeaderController.this.selectedCountry.get();
				if (selectedCountry != null)
				{
					final Map<String, ? extends Language> languages = selectedCountry.getCountry().getLanguages();
					rv = languages != null ? languages.get(iso2Code) : null;
				}
			}
			return rv;
		}

		@Override
		public String getAsString(final FacesContext context, final UIComponent component, final Object value)
		{
			final Language language = value instanceof Language ? (Language) value : null;
			return language != null ? language.getIso2Code() + " - " + language.getOriginalName() : null;
		}
	}

// Controller Methods --------------------------------------------------------------------------------------------------

	public Collection<CountryExt> getCountries()
	{
		LOG.info("HeaderController: SessionId=" + getSession().getId());
		initCountries();
		final Map<String, CountryExt> countries = this.countries.get();
		return countries != null ? countries.values() : new LinkedList<CountryExt>();
	}

	public CountryConverter getCountryConverter()
	{
		return countryConverter;
	}

	public CountryExt getSelectedCountry()
	{
		return selectedCountry.get();
	}

	public void setSelectedCountry(final CountryExt selectedCountry)
	{
		LOG.info("HeaderController.setSelectedCountry: SessionId=" + getSession().getId() + ", selectedCountry=" + (selectedCountry != null ? selectedCountry.getCountry().getIso2Code() : null));

		this.selectedCountry.set(selectedCountry);
		this.selectedLanguage.set(selectedCountry != null ? selectedCountry.getCountry().getDefaultLanguage() : null);
		updateClientSession();
	}

	public void onCountrySelected(AjaxBehaviorEvent e)
	{
		LOG.info("HeaderController.onCountryChanged: SessionId=" + getSession().getId() + ", event=" + e);
		// Do nothing here because setSelectedCountry() is being executed
	}

	public Collection<Language> getLanguages()
	{
		final List<Language> rv = new LinkedList<>();
		final CountryExt selectedCountry = this.selectedCountry.get();
		if (selectedCountry != null)
		{
			final Map<String, ? extends Language> languages = selectedCountry.getCountry().getLanguages();
			if (languages != null)
			rv.addAll(languages.values());
		}
		return rv;
	}

	public LanguageConverter getLanguageConverter()
	{
		return languageConverter;
	}

	public Language getSelectedLanguage()
	{
		return selectedLanguage.get();
	}

	public void setSelectedLanguage(final Language selectedLanguage)
	{
		LOG.info("HeaderController.setSelectedLanguage: SessionId=" + getSession().getId() + ", selectedLanguage=" + (selectedLanguage != null ? selectedLanguage.getIso2Code() : null));

		this.selectedLanguage.set(selectedLanguage);
		updateClientSession();
	}

	public void onLanguageSelected(AjaxBehaviorEvent e)
	{
		LOG.info("HeaderController.onLanguageSelected: SessionId=" + getSession().getId() + ", event=" + e);
		// Do nothing here because setSelectedLanguage() is being executed
	}

// Internal Logic ------------------------------------------------------------------------------------------------------

	protected void initCountries()
	{
		if (this.countries.get() == null)
		{
			try
			{
				final CountryDao countryDao = BackEnd.getDefaultBackend().getCountryDao();
				final Map<String, ? extends Country> allCountries = countryDao.getAllCountriesOrderedByIso2();
				final LinkedHashMap<String, CountryExt> extCountries = new LinkedHashMap<>();
				CountryExt selectedCountry = null;
				for (Map.Entry<String, ? extends Country> country : allCountries.entrySet())
				{
					final CountryExt countryExt = new CountryExt(country.getValue());
					extCountries.put(country.getKey(), countryExt);
					if (selectedCountry == null)
						selectedCountry = countryExt;
				}
				if (this.countries.compareAndSet(null, extCountries))
				{
					this.selectedCountry.set(selectedCountry);
					this.selectedLanguage.set(selectedCountry != null ? selectedCountry.getCountry().getDefaultLanguage() : null);
				}
			}
			catch (PersistenceException x)
			{
				LOG.error("Cannot init countries: sessionId=" + getSession().getId());
			}
		}
	}

	protected void updateClientSession()
	{
		final HttpSession httpSession = getSession();
		if (httpSession != null)
		{
			final ClientSessionDao clientSessionDao = BackEnd.getDefaultBackend().getClientSessionDao();
			final ClientSession clientSession = clientSessionDao.getClientSession(httpSession.getId());
			if (clientSession != null)
			{
				final CountryExt selectedCountry = this.selectedCountry.get();
				clientSession.setSelectedCountry(selectedCountry != null ? selectedCountry.getCountry() : null);
				clientSession.setSelectedLanguage(selectedLanguage.get());
			}
		}

		final Application application = getFacesApplication();
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	//private final AtomicReference<ClientSession> clientSession = new AtomicReference<>();

	private final AtomicReference<Map<String, CountryExt>> countries = new AtomicReference<>();
	private final CountryConverter countryConverter = new CountryConverter();
	private final LanguageConverter languageConverter = new LanguageConverter();
	private final AtomicReference<CountryExt> selectedCountry = new AtomicReference<>();
	private final AtomicReference<Language> selectedLanguage = new AtomicReference<>();
}
