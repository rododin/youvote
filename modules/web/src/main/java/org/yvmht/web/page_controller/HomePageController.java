/*
 * HomePageController.java
 */

package org.yvmht.web.page_controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yvmht.web.page_controller.utils.Constants;

/**
 * The backed bean for <code>homePage.xhtml</code>.
 * @author Rod Odin
 */
@SessionScoped
@ManagedBean(name = "homePage")
public class HomePageController
	extends AbstractController
{
// Constants -----------------------------------------------------------------------------------------------------------

	public final static Logger LOG = LoggerFactory.getLogger(Constants.class);
	public static final String DEFAULT_RESOURCE_BUNDLE_NAME = HeaderController.class.getPackage().getName() + ".CommonPageText";

// Constructors --------------------------------------------------------------------------------------------------------

	public HomePageController()
	{
		super (DEFAULT_RESOURCE_BUNDLE_NAME);

	}

}
