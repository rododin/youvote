package org.yvmht.web.page_controller.utils.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.yvmht.web.page_controller.utils.Constants;

/**
 * It is uncompleted and unused for now.
 * @author Rod Odin
 */
@Deprecated
@FacesConverter(value="countryConverter")
public class CountryConverter
	implements Converter, Constants
{
// Constructors --------------------------------------------------------------------------------------------------------

	public CountryConverter()
	{
	}

// Methods -------------------------------------------------------------------------------------------------------------

	@Override
	public Object getAsObject(final FacesContext context, final UIComponent component, final String value)
	{
		return null;
	}

	@Override
	public String getAsString(final FacesContext context, final UIComponent component, final Object value)
	{
		return null;
	}
}
