package org.yvmht.web.page_controller.utils.filters;

/**
 * Created by IntelliJ IDEA.
 * User: arck  * Date: 23.12.11 * Time: 0:07
 */
public interface Criteria {
	// Constants ---------------------------------------------------------------------------------------

	/**
	 * Records inclusion/exclusion mode.
	 */
	public enum RecordsMode
	{
		/**
		 * Records must be included into the result list if they meet filter conditions.
		 */
		INCLUDE,
		/**
		 * Records must be excluded from the result list if they meet filter conditions
		 * or by other word, records will be included into the result list if they doesn't meet
		 * the filter conditions..
		 */
		EXCLUDE
	}

	/**
	 * Multiple fields accounting mode (AND, OR, XOR).
	 */
	public enum FieldsMode
	{
		/**
		 * Records included into the result list must meet all the fields provided in the filter conditions.
		 */
		AND,
		/**
		 * Records included into the result list must meet at least one of the fields provided in the filter conditions.
		 */
		OR,
		/**
		 * Record included into the result list must meet the only one fild (no other fields) provided in the filter conditions.
		 */
		XOR
	}

	/**
	 * Values definition mode (COMPLETE, PARTIAL).
	 */
	public enum ValuesMode
	{
		/**
		 * Records must be included into the result list if their field(s) value(s) are exactly equal
		 * to the value(s) provided in the filter conditions.
		 */
		COMPLETE,
		/**
		 * Records must be included into the result list if their field(s) value(s) are beginning
		 * with the value(s) provided in the filter conditions.
		 */
		PARTIAL
	}

	/**
	 *  Character comparison mode (CASE_SENSITIVE, CASE_UNSENSITIVE)
	 */
	enum CaseSensitivity
	{
		/**
		 * Strings will be compared using case sensitive way.
		 */
		CASE_SENSITIVE,
		/**
		 * Strings will be compared using case unsensitive way.
		 */
		CASE_UNSENSITIVE
	}

// Methods -----------------------------------------------------------------------------------------

	public RecordsMode getRecordsMode();
	public void setRecordsMode(RecordsMode recordsMode);
	public FieldsMode getFieldsMode();
	public void setFieldsMode(FieldsMode fieldsMode);
	public ValuesMode getValuesMode();
	public void setValuesMode(ValuesMode valuesMode);
	public CaseSensitivity getCaseSensitivity();
	public void setCaseSensitivity(CaseSensitivity caseSensitivity);


}// public interface Criteria

// EOF
