package org.yvmht.web.page_controller.utils.filters;

/**
 * Created by IntelliJ IDEA.
 * User: arck  * Date: 23.12.11 * Time: 0:09
 */
public interface Comparator <ObjectType, FilterValue extends ObjectType> {
// Constants ---------------------------------------------------------------------------------------

// Methods -----------------------------------------------------------------------------------------

	/**
	 * Checks if the given <code>object</code> meets the <code>criteria</code> in the
	 * given <code>filterValue</code>.
	 *
	 * @param object the object to be checked
	 * @param filterValue the etalon value to be checked with
	 * @param criteria a set of rules defining how to check
	 * @return <code>true</code> if the <code>object</code> meets the <code>criteria</code>
	 *         in the <code>filterValue</code>, <code>false</code> otherwise.
	 */
	public boolean compare(ObjectType object, FilterValue filterValue, Criteria criteria);
}
