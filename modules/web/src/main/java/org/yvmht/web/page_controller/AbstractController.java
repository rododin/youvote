/*
 * BaseController.java
 */

package org.yvmht.web.page_controller;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yvmht.web.page_controller.utils.Constants;

/**
 * The Basic class for all managed beans.
 *
 * @author Nikolay Chebotaryov
 */
public abstract class AbstractController
	implements Serializable, Constants
{
// Constants -----------------------------------------------------------------------------------------------------------

	public final static Logger LOG = LoggerFactory.getLogger(AbstractController.class);
	//final Application application = getFacesApplication();
	//final Locale appDefLocale = application.getDefaultLocale();

// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractController(String defaultResourceBundleName)
	{
		assert defaultResourceBundleName != null;
		assert !defaultResourceBundleName.isEmpty();

		final Application application = getFacesApplication();
		final Locale appDefLocale = application.getDefaultLocale();
		try
		{
			final ExternalContext externalContext = getExternalContext();
			final Locale requestLocale = externalContext.getRequestLocale();
			defaultResourceBundle = ResourceBundle.getBundle(defaultResourceBundleName, requestLocale != null ? requestLocale : appDefLocale);
			final Locale resourceBundleLocale = defaultResourceBundle.getLocale();
			LOG.info("LOCALES: appDefLocale=" + appDefLocale + ", requestLocale=" + requestLocale + ", resourceBundleLocale=" + resourceBundleLocale);
		}
		catch (Exception x)
		{
			LOG.error("Unable to retrieve default ResourceBundle: defaultResourceBundleName. Forcing English locale");
			defaultResourceBundle = ResourceBundle.getBundle(defaultResourceBundleName, appDefLocale != null ? appDefLocale : Locale.forLanguageTag("en"));
		}
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

// Misc. methods -------------------------------------------------------------------------------------------------------

	public static FacesContext getFacesContext()
	{
		return FacesContext.getCurrentInstance();
	}

	public static ExternalContext getExternalContext()
	{
		return getFacesContext().getExternalContext();
	}

	public static Application getFacesApplication()
	{
		return getFacesContext().getApplication();
	}

	public static HttpSession getSession()
	{
		HttpSession rv = null;
		final Object so = getExternalContext().getSession(false);
		if(so != null && so instanceof HttpSession)
			rv = (HttpSession)so;
		return rv;
	}

	public static UIParameter findChildParameter(ActionEvent event, String name)
	{
		UIParameter rv = null;
		final UIComponent component = event.getComponent();
		final List children = component.getChildren();
		for(Object o : children)
		{
			//LOG.debug("Child: " + o);
			if(o instanceof UIParameter)
			{
				UIParameter p = (UIParameter)o;
				//LOG.debug("Param name : " + p.getName());
				//LOG.debug("Param value: " + p.getValue());
				if(p.getName() != null && p.getName().equals(name))
				{
					rv = p;
					break;
				}
			}
		}
		return rv;
	}

	public ResourceBundle getDefaultResourceBundle()
	{
		return defaultResourceBundle;
	}

	public String getMessage(String messageKeyInDefaultMessageBundle)
	{
		assert messageKeyInDefaultMessageBundle != null;

		final ResourceBundle rb = getDefaultResourceBundle();
		assert rb != null;

		return rb.getString(messageKeyInDefaultMessageBundle);
	}

	public String formatMessage(String messageKeyInDefaultMessageBundle, Object... args)
	{
		final String message = getMessage(messageKeyInDefaultMessageBundle);
		assert message != null;

		return new MessageFormat(message).format(args);
	}

// Fields --------------------------------------------------------------------------------------------------------------

	private ResourceBundle defaultResourceBundle;
}
