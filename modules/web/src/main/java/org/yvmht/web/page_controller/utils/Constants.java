package org.yvmht.web.page_controller.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Declares a set of useful constants.
 *
 */
public interface Constants
{
// Constants -----------------------------------------------------------------------------------------------------------

	public final static Logger LOG = LoggerFactory.getLogger(Constants.class);
}

